<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

![header image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI.-2019WALLPAPER.jpg)

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

_Updated: 1/27/2021_

_Analysis of 2019 data is ongoing. We will update this report as we complete sections_

_This page summarizes results from 2019. At the beginning of each section there are links to more detailed analysis briefs_

## Sections

* [Introduction](#introduction)
* [Methods](#methods)
* [Sample and Data Collection](#sample-and-data-collection)
  * [Survey Design](#survey-design)
  * [Implementation](#implementation)
  * [Lessons Learned](#lessons-learned)
* [Results](#results)
  * [Description of Variation](#description-of-variation)
  * [Sources of Variation](#sources-of-variation)
  * [Predicting Variation with Spectroscopy](#predicting-variation-with-spectroscopy)
  * [Predicting Varation with Taste](#predicting-variation-with-taste)
* [Conclusions](#conclusions)
  * [Next steps](#next-steps)

## Introduction
Nutrient density is most commonly defined as the level of nutrients per unit calorie. For example, kale has on average a relatively high level of nutrients but a low level of calories. By this metric then kale has a high nutrient density score. Conversely, rice has many more calories in it per unit nutrient and so would have a low nutrient density score. Soda would be a “food” with very many calories and almost no nutrients and so has an extremely low score according to this definition of nutrient density. Using this definition there is an implicit assumption that all kale or rice is relatively nutritionally uniform.

There is, however, another definition of nutrient density which is beginning to be understood by the broader food movement. While not completely defined as of yet, when we talk about Nutrient Density, what we mean is how nutritious one bunch of kale is in relation to another. Or one bag of rice to another. 

The [Real Food Campaign](https://lab.realfoodcampaign.org/) (RFC) was founded to deepen our understanding of this new definition of nutrient density. The RFC emerged from a collaboration between the [Bionutrient Food Association](https://bionutrient.org) and its membership, [Next7](https://www.next7.org/) and [Our Sci LLC](https://www.our-sci.net/) in 2018 with four primary goals:  
1. Determine the amount of variation in nutrition in the food supply.  
2. Relate soil health and nutrient density outcomes to crop and soil management practices.  
3. Predict nutritional parameters in produce using spectral data and metadata.  
4. Build a public library of crop nutrition and soil and crop management data.

In its first year of operation, the RFC showed that there was significant variation (up to 200:1) in antioxidants, polyphenols and minerals in carrots and spinach (see 2018 Final Report). So, one carrot may not be equal to another carrot. The RFC also found that spectral reflectance data could be used to correctly identify high (>50% percentile) antioxidant and polyphenol content 73 - 86% of the time. However, the RFC did not collect sufficient crop and soil management data to identify relationships between management and nutrient density outcomes. 

In 2019, the RFC sought to both improve the efficiency and quality of data collection and to expand the number of samples analyzed by the RFC lab. Significant changes to the program in 2019 included:  

- Expanding the total number of samples from 800 (in 2018) to 3000 samples.  
- Expanding the number of crops from 2 (carrots and spinach) to 6 (carrots, spinach, kale, lettuce, cherry tomato and grapes).  
- Increasing the reflectance wavelength range measured from 365 nm - 940 nm in 2018 to 365 - 2100 nm by adding an additional spectrometer to the lab process.  
- Collecting more detailed farm management data by creating a “Farm Partner Program” to work directly with 30-45 farmers to collect both samples and granular management data.  
- Revising lab processes based on lessons learned in 2018 to reduce major sources of noise and eliminate unnecessary tests.

Follow these links for more details about the [goals and improvements to the 2019 program](https://lab.realfoodcampaign.org/rfc-food-soil-survey-2019/) and [sample collection best practices](https://lab.realfoodcampaign.org/sample-best-practices-2019/).

> While this document is called the **2019 Final Report**, it is important to note that all of the data used to generate this report is publicly available so anyone can use this dataset to ask novel research questions. In fact, the RFC is seeking partners who can bring unique perspectives and expertise to the data to help learn how we can improve nutrient density in food

## Methods
### Lab Methods
The Real Food Campaign lab performs lab tests on both soil and food.  The goal is to identify lab tests which are inexpensive (low capital cost, low ongoing cost), capture the broadest perspective on quality (classes of compounds rather than individual compounds), and may correlate to easier-to-measure parameters like spectral reflectance.  This informs what test we run.

Data and Farm partners who submitted produce and soil samples to the RFC lab completed full surveys for each sample in the field, which included (depending on sample type) store information, farm information, management data, as well as visual and taste evaluations.  This information was collected prior to samples being received in the lab. Full details about sample metadata collection are available in the [Sample and Data Collection](#sample-and-data-collection) section below.

#### Produce
- Surface reflectance using the Bionutrient Meter (10 wavelengths, 365nm to 940nm) and Si Ware’s Neospectra (1350 – 2500nm).
- Juice reflectance using the Bionutrient Meter and Si Ware’s Neospectra.
- Juice Brix (using a 0 – 32% optical Brix meter).
- Antioxidants using the [FRAP method](https://docs.google.com/document/d/1pVwZtnIiM0NiZmrbfESFRfK093fLhVw8pZGAe1OQKWc/edit?usp=sharing), modified from this [assay kit](https://www.cellbiolabs.com/sites/default/files/STA-859-frap-assay-kit.pdf). 
- Polyphenols using a modified [Folin-Ciocalteu total phenolics assay](https://docs.google.com/document/d/1aWpAP-O4-tgrsnRIpMqlexSNw-IVwQEGijYPBYtPido/edit?usp=sharing).
- Minerals using [XRF](https://docs.google.com/document/d/1EIzBBHt9ZVml__6RC6gw1jn7-Y53i4g266GWi8w_JHo/edit?usp=sharing).

#### Soil
- Surface reflectance using the Bionutrient Meter and Si Ware’s Neospectra.
- pH using pH teest strips.
- Total organic carbon using [loss on ignition (LOI) method](https://onlinelibrary.wiley.com/doi/abs/10.1111/ejss.12224)
- Carbon mineralization (ie soil respiration) using 24hr burst method (see [comparable method](https://solvita.com/co2-burst/))
- Minerals using [XRF](https://docs.google.com/document/d/1EIzBBHt9ZVml__6RC6gw1jn7-Y53i4g266GWi8w_JHo/edit?usp=sharing).


### Statistical approaches
The RFC is a large observational study that depends on volunteers and citizen scientists to submit samples and sample metadata to the lab. As such, the data is often not normally distributed, which is a requirement for many common statistical approaches. Therefore, the RFC uses a combination of traditional approaches (regression, ANOVA, etc) and more novel, non-parametric methods. At the beginning of each section, there is a link to a more detailed analysis brief, specific details about statistical approaches are available in those briefs. 


## Sample and Data Collection
### Survey Design
**Detailed Brief:** [Community Partner Sampling](https://gitlab.com/our-sci/real-food-campaign/rfc-docs/-/wikis/Community-Partner-Sampling)

The RFC relies on a very active and passionate community to provide samples to the RFC lab to drive the campaign. In order to effectively engage with this community at a scale large enough to meet our sample goals and increase the amount of granular data collected, we needed:

1. Easy-to-use data collection tools that promoted consistent data entry.
1. Automated data management systems to better track community activity and sample flow from the field to the RFC lab.
1. Traceability so that lab results could be traced back to specific management practices in the field.

In 2018, data collection was done using paper surveys, which created a number of issues: 1) the data needed to be entered into a computer manually, requiring more labor hours per sample; and 2) there can be transcription errors when manually entering data due to mis-reading answers, dirty or wet forms, or hard to read handwriting. To address these issues and meet the data management requirements laid out above, we released the *RFC Collect* Android Application. This Android-based app allowed RFC staff to create standardized data collection surveys to capture key sample data and eliminated the need for paper surveys.

In order to capture more granular farm management data while still analyzing a diverse set of samples from stores, farmers markets and farms across the country, the RFC set up two distinct community partner programs in 2019:

- **Farm Partners Program**: The RFC would work directly with farmers who were willing to share management and variety data and to send samples into the RFC lab. The data collected by these partners would be much more detailed than what was collected in 2018 or what was collected by Data Partners.
- **Data Partners Program**: Similar to the 2018 Data Partners Program, data partners could be anyone who was interested in submitting samples to the RFC lab. Data partners would submit samples from stores, farmers markets and farms in their area.

#### Farm Partners Program
The RFC’s goal was to recruit 30 - 40 Farm Partners. Recruitment was done through a combination of an informational booth at the BFA Soil and Nutrition Conference in November, 2018 and through the BFA newsletter. After receiving interest from 103 farmers, we enrolled 34 Farm Partners. Additionally, we collaborated with 10 farmers from the [Massachusetts chapter of NOFA](https://www.nofamass.org/), bringing the total number of participating farmers to 44. All participating farmers were trained to use the *RFC Collect App* for data collection, given a 1-year subscription to [FarmOS](https://farmos.org/) (an open-source farm management platform) and were provided sample packets that included pre-labelled sample bags, a soil probe, an Android phone (if needed) and postage-paid shipping envelopes. The RFC worked directly with FarmOS, with support from [OpenTEAM](https://openteam.community/), to build integrations in the *RFC Collect* app, so that Farm Partners could push and pull data between their FarmOS and *RFC Collect* accounts. 

In short, for every sample submitted, Farm Partners:  
1. Mapped the field in FarmOS. These fields were then available to be referenced in the *RFC Collect* app so the RFC could trace each sample back to a specific field.  
2. Completed a “Planting Form - RFC Farm Partners” survey that captured key crop management data such as tillage, addition of amendments and whether the crops were planted in the field directly from seed or transplanted.  
3. Recorded post-planting management by completing the “In Season Management Form - RFC Farm Partners” survey once per crop or multiple “Weekly Management Form - RFC Farm Partners” surveys during the growing season.  
4. At harvest samples and metadata were collected. Each sample included the crop and two soils samples--in depths increments of 0-10 cm and 10-20 cm--from within 12 inches of the crop sample. A “Sample Collection Survey” was completed to capture key sample metadata (crop variety and the sample number from the provided sample bag) and shipped the sample to the RFC lab. 

#### Data Partner Program

In order to capture variability across food supply chains and meet our goal of analyzing 3,000 food samples, we needed volunteers who were willing to submit samples from stores, markets and farms from across the USA on a weekly or bi-weekly basis. The RFC recruited 26 volunteers, called Data Partners, from the BFA Soil and Nutrition Conference (2018 and 2019) and the BFA Newsletter in three cohorts throughout the year. Data Partners were provided with a sample packet, including an Android phone (if needed), a soil probe, pre-labelled sample bags and postage-paid shipping envelopes. A detailed sampling plan was developed for each Data Partner to provide some guidance and to ensure that the RFC was receiving food from a diversity of management, source and produce types. When sampling, Data Partners completed a “Sample Collection Survey” which captured key sample metadata. If samples came from a store, metadata included labelling information (certified organic, greenhouse-grown, etc). If the Data Partner visited a farmers market or farm, then sample metadata included general farm practices (organic, no-till, cover cropping, etc), use of irrigation and types of amendments used (mulch, compost, fertilizer, etc).

### Implementation
**Detailed Brief:** [Sample Sources and Quality](https://gitlab.com/our-sci/real-food-campaign/rfc-docs/-/wikis/Sample-Sources-and-Quality)

#### Sample Submissions
The RFC lab received 2,095 samples from across the USA in 2019, with 813, 230 and 988 samples coming from farms, farmers markets and stores, respectively (Table 1). While the samples collected fell short of our goal of 3,000 samples, the RFC was able to source samples from a wide variety of states, farms, stores, and store brands. The total number of samples for each crop type and source of samples are outlined in Figure 1.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%201.png)

Of the 44 Farm Partners who enrolled in the project, 30 farmers submitted samples to the RFC lab. These partners submitted 438 samples, accounting for 54% of the samples submitted from farms, and sent samples to the RFC lab regularly throughout the 2019 season (Figure 2). Initially, Farm Partners submitted planting management data for 80% and post-planting management data for 65% of those samples. However, using email to follow up with Farm Partners, the RFC received detailed planting and post-planting management data for 92% and 87% of those samples, respectively. Data Partner cohorts were active for about 2 months before sample submissions declined. In total, the RFC received samples from 26 Data Partners, from three cohorts, throughout the 2019 season. Working directly with farmers helped the RFC to collect detailed crop variety data, with 92.4% of samples arriving from Farm Partners having variety data compared to 79.2% of farm samples from Data Partners. Likewise, variety data was only available for 21.7% and 18.7% of samples from farmers markets and stores, respectively.

On average, 92.7% of samples that arrived at the RFC lab were of good quality, meaning that they were not rotten or physically damaged. Both temperature and sample source had a noticeable impact on sample quality, with only 84.2% of samples submitted between July and September being good enough to analyze compared to 96% of samples received in June or October - December. Additionally, 96.6% of samples sourced from stores and farmers markets arrived at the RFC lab in good quality compared to 86.3% of samples from farms. 

**Figure 2.** Weekly sample submissions by Farm and Data Partners.
![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/sample_packages.png)

### Lessons Learned
After the Farm and Data Partner Programs were completed, evaluations were sent out to all participants. A detailed review of partner feedback is available [here](https://gitlab.com/our-sci/real-food-campaign/rfc-docs/-/wikis/Community-Partner-Sampling), but a few key lessons learned include:  

1. Using Android phones provided by the RFC, for non-Android users, was a significant hindrance to participation. A cross-platform app, that works on partners computers or personal phones, was needed to improve data submission and participation.  
2. Improving training, including one-on-one phone calls, webinars and a handbook is needed to help partners navigate the data collection app, collect samples, and approach neighboring farmers or farmers at farmers markets to collect samples and management data.  
3. Reducing the time commitment for Data Partners, who could spend hours driving to farms or farmers markets to collect samples, would improve participation.  

## Results
### Description of Variation
**Detailed Briefs:** 

- [Summary statistics and Histograms](https://octavioduarte.gitlab.io/public-briefs/summary)
- [XRF Brief](https://octavioduarte.gitlab.io/public-briefs/xrfReview)

The median concentration and the max:min ratio of antioxidants, polyphenols and brix are presented in Table 2 for each crop. There was significant variation across nutrients and crops measured in 2019, with the max:min ratio ranging from 3 to over 360. Similarly, there was significant variation in the soils received by the RFC lab. Soil carbon values ranged from 1 - 15% carbon and from 1 to 76 ug C per gram of soil (Table 3).  

**Table 2.** Median and max/min ratio for antioxidants, polyphenols and brix for each crop and measured.![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%202.png)

**Table 3.** Summary statistics for soil carbon and soil respiration.![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%203.png)

***Note:** Detailed histograms and summary statistics data for minerals measured by the RFC lab are available in the [XRF Brief](https://octavioduarte.gitlab.io/public-briefs/xrfReview). For the purposes of this report, we have chosen to focus on relative mineral values (percentiles) and not on absolute values. The XRF Brief contains a detailed explanation of both the reasoning behind this decision and the data underpinning it.* 

### Sources of Variation
**Detailed Briefs:**

- [Boxplots of nutrient variability](https://octavioduarte.gitlab.io/public-briefs/boxplotsBrief)
- [Relationships between nutrient quality and management](https://octavioduarte.gitlab.io/public-briefs/qualityRelationships)
     - [Farm Practices](https://octavioduarte.gitlab.io/public-briefs/practicesTable.html)
     - [Amendments](https://octavioduarte.gitlab.io/public-briefs/amendmentsTable.html)
- [Relationships between soil quality and management](https://octavioduarte.gitlab.io/public-briefs/soilQualityRelationships)
     - [Farm Practice Table](https://octavioduarte.gitlab.io/public-briefs/soilPracticesTable)
     - [Amendments Table](https://octavioduarte.gitlab.io/public-briefs/soilAmendmentsTable)
- [XRF Brief](https://octavioduarte.gitlab.io/public-briefs/xrfReview)


#### Challenges in understanding variation
In 2019, the RFC received more samples from a wider diversity of sources with more sample metadata included than in 2018. This allowed for a much more detailed exploration of the sources of variation within foods than was possible in 2018. Some of the key drivers of variation that we collected metadata on in 2019 included: climate region, crop color, crop variety and farm practice. Additionally, soil samples taken near each crop sample were analyzed to determine the impact of soil health on nutrient density.

> We are not assuming that climate region, crop color, variety, management practices and soil properties are the only sources of variation in nutrient density, but rather that these are the factors for which we were able to collect meaningful data

Separating out the causes of variation in a large observational study such as this, where much of the data is not normally distributed, can be very difficult. This means that it is often difficult to apply traditional statistical methods to determine what sources of variation may be due to random chance and which ones are real. For example, by looking at the boxplot of variation of antioxidant content in carrots from different climate regions, it appears that carrots from the ‘South’ region are superior to carrots from other parts of the country (Fig. 3, left). Upon closer examination, we can see that the RFC lab only received 3 carrots from the South region and those three carrots were purple. The box plot on the right shows the variation in carrot antioxidant content by color. While the RFC lab only analyzed 19 purple carrots in 2019, results from those carrots and from 2018 show that purple carrots had greater antioxidant content than orange carrots. So, it is much more likely that the reason for the high antioxidant content in carrots from the south was because those samples happened to be purple (random chance) and not because Southern carrots are more nutritious. 

More boxplots showing the effects of climate region, crop color, sample source and crop variety for all 6 crops are available [here](https://octavioduarte.gitlab.io/public-briefs/boxplotsBrief). In many cases, the number of samples from different crop varieties or climate regions were not sufficient to explore their impacts on nutrient density. In 2020, a more intentional sample design, to get samples that are more evenly distributed across climate regions and soil types should be implemented to better understand their effects on nutrient density. Other sources of variation, like crop variety, may require analyzing more samples than is feasible, especially in crops with significant genetic variation (lots of varieties). In future years, we may need to design more structured comparison trials to separate the effects of genetic diversity from other sources of variation.

During the rest of this section, we will be using a variety of statistical and other approaches to tease out “meaningful” sources of variation. We define “meaningful” as findings which are both reasonably statistically significant and have a reasonably high absolute difference such that the outcome matters in the real world. 

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/figure%203.png)

#### Relationship between farm practices and food quality
In this section we seek to better understand the effect of farm practices on food quality, since this is the area where farmers have the most control over their results. First, we used non-parametric analysis to determine the ‘median shift’ of various farm practices or labels compared to a reference set. For the reference, we used samples that either had no listed farm practices or **none** was selected from the farm practice or label questions of the "Sample Collection Survey". More details about the non-parametric analysis and results are available [here](https://octavioduarte.gitlab.io/public-briefs/qualityRelationships). 

Table 4 displays the effects of farm practice on antioxidant and polyphenol values by crop, with green signifying a positive change and red a negative change. For example, the positive shift of 29.44 for lettuce antioxidants in the **Certified Organic** row means that lettuce labelled as certified organic had 29% greater antioxidant content than lettuce samples in the reference set (no regenerative practices or organic labels selected). While there appear to be many positive effects from regenerative practices, the results are largely inconsistent. For example, no-till positively affects antioxidant content in lettuce, but negatively affects antioxidant content in carrots. These inconsistencies may be based on small sample sizes for individual farm practices or individual crops. 

**Table 4.** More detailed versions of this table are available for [Farm Practices](https://octavioduarte.gitlab.io/public-briefs/practicesTable.html) and [Amendments](https://octavioduarte.gitlab.io/public-briefs/amendmentsTable.html)![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%204.png)

In order to increase our statistical power, we combined all of the crops together to test different practices. First, we used each sample's percentile to normalize across all crops. Effectively, this means we ranked each nutrient from lowest (score = 0) to highest (score = 100) for each crop. So, the carrot with the highest antioxidant content has the same score (100) as the kale with the highest antioxidant content. This puts every sample, regardless of crop type, on a consistent 0-100 scale.  We then used the same non-parametric analysis to compare the samples to the reference median. Figure 4 presents the effects of farm practice on all of the analytes measured, with all crops combined. A shift of 0.1 signifies that a nutrients percentile increased by 10 points. For example, we see that cover cropping caused a 0.1 shift in antioxidant content. This means that the average crop grown with cover crops would increase from the 50th percentile (average) to the 60th percentile (above average). The darker the bar, the more likely the findings are true, so in this case the increase in antioxidant content is both likely to be true and are meaningful in the real world. No graph has all positive or all negative shifts, which is a useful reminder that identifying any limited set of indicators of quality may result in misinterpretation of the data. 

**Figure 4.** ![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/figure%204.png)

Interpretation of complex data like this may be helped by developing index values that aggregate multiple parameters into a single value. However, doing it properly in this context requires more expertise in human nutrition than we have on the RFC team right now. To start the conversation, we decided to develop a test index value called *BQI* or *Bionutrient Quality Index* which incorporates 9 analytes measured by the RFC lab into a single value. The *BQI* values included:

- Antioxidants
- Polyphenols
- Brix
- Magnesium
- Sulphur
- Potassium
- Calcium
- Iron
- Zinc

We included these 6 minerals, out of the 15 minerals measured by the RFC, for two reasons: 1) consuming more of these minerals is generally considered to be better and 2) most people do not consume enough of these minerals in their diet. Figure 5 presents the affects of farm practice on *BQI* using two different methods of weighting the individual components. The graph on the left weights each component equally, so the BQI equals the average of the percentiles for each individual nutrient. Basically, this weighting schema suggests that Mg is just as important to human nutrition as antioxidants. The *BQI* graph on the right splits them into categories of compounds. In this case BQI = ((Antioxidants + Polyphenols) / 2) + Brix + ((Mg + S + K + Ca + Fe + Zn) / 6).

If we evaluate farm practices using the first weighting option, then cover crops, irrigation, non-chlorine water, no spray and no-till all have a meaningful positive impact on nutrition. Conversely, biodynamic, certified organic, hydroponic and greenhouse grown practices have a negative impact, but we do not have strong statistical confidence in those negative impacts. However, weighting the individual components by compound type increased the statistical confidence of many of the farm practices. Using this method, we have strong confidence that cover crops, no spray, no till, and organic practices increase nutrient density. We also have more confidence that biodynamic, certified organic, hydroponic and greenhouse grown practices are correlated with lower nutrient density.

> It is important to remember that we basically made up *BQI*, and the reason we made it up is not too make strong assertions about which nutrients are more important, but as a conversation starter. We are actively seeking engagement with researchers from many disciplines to understand this data. What does nutrient density mean? How can we best measure or predict it? 

**Figure 5.** ![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/figure%205.png)

#### Relationship between soil health and food quality
We used multiple linear regression to examine the correlation between soil health parameters measured by the RFC lab (soil organic carbon, soil respiration and pH) and food quality. Table 5 presents the R2 for each crop nutrient. R-squared, also known as the coefficient of determination, is a statistical measure of how close the data are to the fitted regression line. The closer the R2 is to 1, the more that the model explains all the variability of the response data around its mean. At first glance, the relationship (**R2**) between soil health parameters and individual nutrients is weak. However, after removing the largest outliers and re-running the model (**trimmed R2**) a strong relationship between brix, polyphenols and soil health parameters in leafy greens (spinach, lettuce and kale) emerges. These results suggest that there is a relationship between food nutrition and soil health. 

**Table 5.** Relationship between crop nutrients and soil parameters by crop. ![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%205.png)

Going one step further, we wanted to know which farm practices would increase soil health parameters. All of the regenerative practices listed increased soil respiration, an indicator of soil biological activity. However, every practice except no-till decreased soil organic carbon at 0-10 cm (Table 6). Some of these results may be due to small sample sizes, since the RFC only received soil samples for about 40% of the samples received in 2019. To further explore these relationships, the RFC lab should increase the number of soil samples it receives and analyzes. A more in-depth description of the methods and results are available [here](https://octavioduarte.gitlab.io/public-briefs/soilQualityRelationships).

**Table 6.** The effect of farm practice on soil health parameters. More detailed versions of this table are available for [Farm Practices](https://octavioduarte.gitlab.io/public-briefs/soilPracticesTable) and [Amendments](https://octavioduarte.gitlab.io/public-briefs/soilAmendmentsTable)![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%206.png)

#### Key Findings
1. Farm practices that are widely believed to increase soil biology, such as cover cropping and no-till, appeared to increase the nutrient content of produce.
2. The RFC will most likely need to aggregate multiple parameters into an index value for nutrient density. We hope that strong engagement between the RFC and researchers from many disciplnes can lead to a meaningful measurement of nutrient density. 

### Predicting Variation with Spectroscopy
**Detailed Brief:** [Predicting nutrient content](https://octavioduarte.gitlab.io/public-briefs/dataAnalysis)

#### Methods
Each produce sample analyzed in the RFC lab had its reflection spectra measured with the Bionutrient meter (10 channel spectrometer ranging from 365-940 nm) and the Siware spectrometer (1300-2500 nm range, 30 nm resolution). First, the surface reflectance was scanned and then the sample was juiced and the reflection spectra of the juice was measured (Figure 6). 

These reflection spectra were paired with metadata to develop non-destructive and destructive prediction models. Prediction modelling was done using linear regression and Random Forest, which is an ensemble machine learning method, to predict 1) antioxidant and polyphenol content and 2) to classify food samples as having low (0-25%), medium (25-75%) or high (75-100%) antioxidant or polyphenol content. A detailed analysis of how spectral data was pre-processed, comparing multiple linear regression approaches and measuring variable importance can be found [here](https://octavioduarte.gitlab.io/public-briefs/dataAnalysis). For this report, we will focus on the best performing models for predicting nutrient quality using non-destructive and destructive methods.

- **Non-destructive sampling** included surface spectra, sample color and sample source (farm, farmers market or store). This approach most accurately represents data that could be captured by a consumer at the point of purchase (e.g. at the grocery store). Bumpy, rounded or imperfect surfaces impact predictive capacity.

- **Destructive sampling** uses juicing to create a homogenous liquid for prediction. In these models, we also added brix readings and the same metadata (color and source) that are used in the non-destructive method. This represents a supply chain methodology where the user could destroy a small amount of sample to predict nutrient density.

**Figure 6.** ![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/figure%206.png)

**Table 7** Description of model parameters

|Model Name|Description|
|----------|-----------|
|**Non-spectroscopic**|    |
|Metadata only|color, source|
|Metadata + brix|color, source and brix|
|**Non-destructive**|     |
|Bionutrient meter surface|Bionutrient meter surface spectra|
|Bionutrient meter + metadata|Bionutrient surface spectra + color and source|
|Siware surface|Siware surface spectra|
|Siware surface + metadata|Siware surface spectra + color and source|
|**Destructive**|     |
|Bionutrient meter juice|Bionutrient meter juice spectra|
|Bionutrient meter juice+ metadata|Bionutrient meter juice spectra + color, source and brix|
|Bionutrient meter surface and juice + metadata|Bionutrient meter surface and juice spectra + color, source and brix|
|Siware juice|Siware juice spectra|
|Siware Juice + metadata|Siware juice spectra + color, source and brix|
|Siware surface and juice + metadata|Siware surface and juice spectra + color, source and brix|
|**Full Range**|     |
|Bionutrient meter and Siware surface + metadata|Bionutrient meter and Siware surface spectra + color and source|
|Bionutrient meter and Siware juice + metadata|Bionutrient meter and Siware juice spectra + color, source and brix|
|All data|All spectral and metadata|

#### Results
Table 8 presents the results of predicting antioxidant and polyphenol content by crop and model type. Overall, the models were more successful at predicting antioxidant and polyphenol content in grape and spinach than in carrot, cherry tomato, kale or lettuce, regardless of the specific model inputs. In some cases the model accuracy was very high, with Siware juice + metadata model accuracies greater than 0.7 in many instances. One driver for the increased predictive capacity for these two crops appears to be a relative lack of genetic diversity. There were only 9 and 13 grape and spinach varieties, respectively, represented in the RFC database in 2019. In contrast, the RFC received samples from at least 25 different varieties of carrot, cherry tomato, kale and lettuce, which reduced the predictive capacity of these crops (Figure 7). It is also worth noting that the models were better able to predict polyphenol content compared to antioxidant content in 5 out of 6 crops.

**Table 8.** ![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%208.png)

**Figure 7.**![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/figure%207.png)

Models to predict nutrient class (low, medium and high) were more accurate than models predicting actual quality parameters (Table 9). The Bionutrient meter was almost 60% accurate when using surface scans and 65% accurate when classifying polyphenol content using juice spectra, brix readings and metadata. In general, the Bionutrient meter was as or more successful at classifying antioxidant and polyphenol content than the Siware spectrometer, despite a much lower spectral resolution. Similarly, expanding the spectral range from 365 - 2500 nm by combining the Bionutrient and Siware spectra did not increase the predictive capacity of the models. 

**Table 9.**![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%209.png)

#### Key Findings
1. The Bionutrient meter, combined with appropriately attainable metadata is just as effective at predicting nutrient quality as the much more expensive and complicated benchtop Siware device. 
1. Using variety data may provide a large boost to the predictive capacity of the Bionutrient meter. For this reason, the RFC should put more emphasis on capturing variety data in 2020.
1. The best predictive capacity came when attempting to predict the highest (top 25%) and lowest (bottom 25%) nutrient contents. In this case, the model lumped all the samples in the 25 – 75% range into the same group. This recognizes that the primary goal is to determine if a food is above average, average, or below average.

### Predicting Variation with Taste
**Detailed Brief:** [Relationship between taste and nutrient density](https://octavioduarte.gitlab.io/public-briefs/flavorScalesBrief)

#### Methods
There is significant interest in whether taste or other flavor indicators can be used to predict nutrient density in foods. To test that theory, we provided the opportunity for farm and data partners to taste test the produce they submitted to the RFC lab. At the end of each "Sample Collection Survey", community partners could rate flavor, sweetness and overall taste on a simple 1-5 scale. Community partners completed taste tests on between 39 and 122 samples per crop. Using ordered median tests (more details on the statistical methods are available [here](https://octavioduarte.gitlab.io/public-briefs/flavorScalesBrief), we tested the relationships between the flavor scales and brix, antioxidants, and polyphenols.

#### Results
Table 10 summarizes the directional impact of each indicator x nutrient combination for all 6 crops. No arrow means that there was no significant effect for that indicator x nutrient combination, while directional arrows indicate a positive or negative correlation. For example, the green up arrow in the carrot column and flavor row of the antioxidants section means that a better flavor score was significantly correlated with increased antioxidant content in carrots. In fact, we see that antioxidant content was positively correlated with flavor, taste, and sweetness in carrots, lettuce, and spinach. Brix is routinely used as a measure of dissolved sugars, and so we would expect to find that higher brix values correlate with higher sweetness. That assumption holds true in cherry tomato and grapes, but does not hold true in carrot or kale. Similarly, polyphenol content and brix were negatively correlated to flavor indicators in carrot and kale, but positively correlated to flavor scales in cherry tomato, grapes, lettuce, and spinach. These results suggest that subjective taste indicators do have potential in predicting nutrient quality, but that the relationship between specific nutrients and flavor indicators vary by crop. 

**Table 10.** Relationsips between flavor scales and crop nutrient content![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Reports/2019%20report/table%2010.png)

#### Key Finding
1. Taste can serve as a useful indicator in predicting nutrient content in produce.

## Conclusions
In 2019, we made significant progress in meeting our primary goals.

**Determine and amount of variation in nutrition in the food supply.**

There was significant variation across the samples received by the RFC lab, with the variation often in the 10x to 100x ranges. This variation is caused, in part, by the incredible diversity of samples that the RFC lab received from dozens of states, hundreds of farmers and hundreds of different food brands across the USA. 

**Relate soil health and nutrient density outcomes to crop and soil management practices.**

The programmatic changes made from 2018 to 2019, working directly with farmers and asking much more detailed questions, provided the RFC with the data needed to conduct a much deeper examination of the causes of variation than was possible in 2018. Results from that analysis show that many of the regenerative practices tested increase soil biological activity, and that increased soil bioligical activity increases nutrient content in food. Conversely, practices that are less dependent on soil biology, like hydroponic or greenhouse grown crops, showed a general reduction in nutrient content. The results presented here also reveal the need to develop a more intentional sampling design in 2020 so we can better explore the affects of climate region and genetic variation on nutrient density.

**Predict nutritional parameters in produce using spectral data and metadata.**

The Bionutrient Meter was just as effective at predicting high and low quantities of antioxidants and polyphenols in foods as were more expensive bench top spectroscopy tools. Greater genetic diversity significantly reduced our ability to predict nutritional quality, in part because many samples did not have variety data. In 2020, we need to emphasize collecting more variety data so that we can better understand the impact of variety on nutrient density and improve our predictions of nutrient density.

**Build a public library of crop nutrition and soil and crop management data.**

This report introduces the conversation about what a combined nutrient index would look like. However, we by no means wish to claim that our definition is the one that should be adopted. Rather, we are building a library of publicly available data about nutrient density outcomes that anyone who is interested can explore. We hope that this report leads to community engagement and discussion about nutrient density is, what nutrition means, and how we can improve nutritional outcomes in food.

### Next Steps
In 2020, we will apply the lessons learned from this year to continue to improve our sample collection and lab analysis processes.

- Collect a larger percentage of samples directly from farmers. This will increase our ability to capture key data about genetic variation, soil types and climate regions.
- Develop a cross-platform app and better training tools to make it easier for farm partners to submit management data to the Real Food Campaign.
- Develop a more intentional survey design, with more specific targets for crops and climate regions across the country.
- Modify lab and sample collection and shipping protocols to reduce error within our processes.

