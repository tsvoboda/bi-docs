### Method selection

Bionutrient Institute labs perform lab tests on both soil and food and requires a large number of samples to address our core research goals.  As such we identify lab tests which are
- inexpensive (low capital cost, low ongoing cost)
- capture the broadest perspective on quality (classes of compounds rather than individual compounds)
- and may correlate to easier-to-measure parameters like spectral reflectance

The methods performed have been consistent from 2019 onward but improve and expand year to year.

> _IMPORTANT: To see the most up-to-date methods and our exact procedures, see the *Lab Processes* section in the sidebar._

- [Lab equipment needed](https://docs.google.com/spreadsheets/d/13hsFXMrDMSm6wZBeLG9cz_OGs9uorBCjUjsCay8TqmI/edit?usp=sharing)
- [Plan and crop list](2020_plan.md)

### Produce

- Community partners who are submitting the samples complete full surveys for each sample in the field, which will include (depending on sample type) store information, farm information, management data, as well as visual and taste evaluations. This information is collected prior to samples being received in the lab.
- **Surface reflectance** using Our Sci reflectometer (10 wavelengths, 365nm to 940nm) and Si Ware’s Neospectra (1350 – 2500nm). These scans are done on clean surfaces, without peeling or other destructive sampling techniques (what you could do in a grocery store).
- **Juice reflectance** using Our Sci reflectometer (10 wavelengths, 365nm to 940nm) and Si Ware’s Neospectra (1350 – 2500nm)
- **Juice Brix** (using a 0 – 32% optical Brix meter)
- **Total Antioxidents** using FRAP method (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5164913/)
- **Total Polyphenols** using [Folin-Ciolcalteu method](https://onlinelibrary.wiley.com/doi/10.1002/9781119135388.ch6))
- **Total Proteins** using [Lowry method](https://en.wikipedia.org/wiki/Lowry_protein_assay)
- **Total minerals** using XRF using custom calibrations

### Soils

- Soil samples are collected from 2 depths, 0-4 inches and 4-8 inches, from within 12 inches of the produce sample that was submitted.
- **Surface reflectance** using Our Sci reflectometer (365nm to 940nm) and Si Ware’s Neospectra (1350 – 2500nm)
- **pH** using pH test strips
- **Total organic carbon** using LOI - loss on ignition [based off this method](https://onlinelibrary.wiley.com/doi/abs/10.1111/ejss.12224)
- **Soil respiration** using the 24hr burst method [(see a comparable method here)](https://solvita.com/co2-burst/)
- **Total minerals** using XRF using custom calibrations
