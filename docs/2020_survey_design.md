**Table of Contents**

1. [Goals](#goals)
2. [Lessons learned from previous years](#lessons-learned-in-previous-years-of-the-rfc)
3. [2020 RFC Survey Design](#2020-rfc-survey-design)
4. [Methodology](#methodology)


## **Goals**
The RFC Food/Soil survey serves to:

1. Identify the amount of variation in nutrition in the food supply
1. Attempt to relate nutritional data to field-measurable spectral data
1. Relate soil health and nutrient density data to crop management practices
1. Create a data pipeline that BFA members and partners can utilize to answer questions of interest.

The 2020 Food/Soil survey will target 3000 – 4000 samples from [17 produce types](#crop-targets-from-farms-and-stores).  All samples will also have reflectance measurements from 350nm to 2100nm to attempt to calibrate the lab data with the spectral data.


## **Lessons learned in previous years of the RFC**

* A key lesson from 2018 was that we needed more detailed information about crop management if we wanted to identify links between crop management, soil health and nutrient density. 
* In 2019, we created the **Farm Partners Program** to work directly with farmers to capture granular crop management data. Details of the 2019 Farm Partners program and lessons learned are available [here](2019-Final-Report#farm-partners-program)

     - We received 451 food/soil samples from 30 farmers across the USA, approximately 25% of the total samples received.
     - Farmers submitted detailed management data for 92% of these samples(2019-Final-Report#capturing-management-data).
     - We were only able to get detailed variety/cultivar data when an RFC volunteer worked directly with the growers.

## **2020 RFC Survey Design**
A key change from previous years is a more intentional survey design, with more specific targets both for each crop and for different regions across the country.

### Changes in 2020
* Identified approaches to improve the Farm Partners program to make the process of submitting management data and samples to the RFC lab easier. These improvements include a cross-platform data entry platform and better training modules.
* [Identified and addressed sources of sample loss during shipping](https://our-sci.gitlab.io/bionutrient-institute/bi-docs/#conclusions).


#### Crop targets from farms and stores
* Increase samples analyzed from 2000 in 2019 to 4000 in 2020.
* In 2020, we have expanded the crop list from 6 crops in 2019 to 17 (below).
* Increase the percentage of samples received directly from Farm Partners from 25% in 2019 to 75% in 2020. 
* We are targeting 120 Farm Partners for vegetable samples, up from 30 in 2019. We also hope to recruit an additional 100-150 farms to submit grain samples.

| Crop | Total Samples | Farm Samples | Store Samples |
| ------ | ------ | ------ | ------- |
| **Returning Crops from 2019** | | | |
| Carrots | 100 | 100 | 0 |
| Spinach | 200 | 200 | 0 |
| Head Lettuce | 100 | 100 | 0 |
| Kale | 100 | 100 | 0 |
| Grape | 200 | 150 | 50 |
| **New Crops in 2020** | | | |
| Beets | 500 | 350 | 150 |
| Bok Choy | 171 | 114 | 57 |
| Leeks | 100 | 75 | 25 |
| Mizuna | 171 | 114 | 57 |
| Mustard greens | 171 | 114 | 57 |
| Potatoes | 600 | 400 | 200 |
| Sweet Peppers | 100 | 75 | 25 |
| Swiss Chard | 100 | 75 | 25|
| Yellow Squash | 200 | 150 | 50 |
| Zucchini | 200 | 150 | 50 |
| **New Grains in 2020** | | | |
| Oats | 250 | 250 | 0 |
| Wheat | 250 | 250 | 0 |
| **Total** | **3513** | **2767** | **746** |

#### Targetting variations in climate region, soil type and farm type.
1. We are using [NOAA climate regions](https://www.ncdc.noaa.gov/monitoring-references/maps/us-climate-regions.php) as a basis for targetting farm partners across the country.
1. We developed an onboarding dashboard to help target Farm Partners and crops based on climate region, soil type and farm type. This [dashboard](https://app.our-sci.net/#/dashboard/by-dashboard-id/rfc-2020-onboarding) provides RFC staff with real-time, detailed information about which types of samples have been pledged, so that we can identify sampling gaps and target specific regions and farm types.

## **Methodology**

The RFC relies on a very active and passionate community to provide samples to the RFC lab to drive the campaign. In 2020 we will recruit volunteers for 2 partner programs:

 * Grower Partner Program: RFC staff work directly with farmers, who provide detailed management data and food/soil samples to the RFC lab and in return receive detailed lab analysis of their samples. 

 * Community Partner Program: Citizen scientists submit samples from grocery stores, farmers markets and farms across the USA.

### Sample Collection Process

The RFC provides community partners with pre-addressed and pre-paid shipping containers. Once samples are collected, they are shipped using standard USPS Priority Legal sized envelopes (ships 1 – 3 days) on the same day they were collected.  This minimizes reduction in quality like antioxidents and other compounds which are known to decrease quickly after harvest.  Each sample envelope is designed to contain 3 samples with each sample including 1 food sample and 2 soil samples (at 0 – 4” and 4 – 8”) from within 12 inches of where the crop was grown. 

Grower and Community Partners fill out web-based surveys using the Real Food Campaign Collect app when collecting the sample and submit it electronically.  That will be connected with the lab data to provide a full accounting of the sample. Grower Partners are also expected to fill out detailed management activity surveys at planting and at regular intervals throughout the growing season, providing detailed information on tillage practices, the use of mulch and other amendments and weed, pest and disease control.

To see detailed descriptions of the testing methods, see the [Lab Testing Page](Lab-Testing-2020).











 
