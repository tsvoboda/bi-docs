# Data Explorer

Visit the [Data Explorer](https://app.surveystack.io/static/pages/dataExplorer?explained=polyphenols) to learn more about the library of crop nutrition that we've collected, compare results, and apply filters relevant to you.

![Data Explorer - Logged In](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/dataexplorer/datadashoverview.png)

## FAQs 

<iframe class="embedded-content" height="350" src="https://our-sci.gitlab.io/bionutrient-institute/tutorials/viewing_data/#dashboard-faq"></iframe>



