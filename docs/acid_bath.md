# Acid Bath for Glass Culture Tubes

**Important note**:  Wear appropriate PPE, this includes safety goggles, lab coat, nitrile gloves + over the cuff chemical resistant gloves. Take extra care when working with acid. Avoid spills, dripping liquid down the sleeves, and inhaling acid fumes. Work in an extraction hood if it’s available.  

Make ~10% vol HCl by adding 225 mL of DI to a squeeze bottle, then add 25 mL HCl. ALWAYS ADD ACID TO WATER.  
 
Use an acid bath to clean the accumulated glass test tubes. Fill a plastic tub (ensure you use a tub that is made with an acid resistant material such as polypropylene) to cover the tubes with DI.  Add ~100 mL of the 10% HCl. Mix to spread the acid throughout the tub.  Take care here to avoid splashing or spilling. You can let the dirty glass sit overnight in the tub. This bath can be used multiple times as long as it doesn’t appear dirty. Dispose of based on local regulations.  

To do DI rinses, fill a clean tub with DI. Grab handfuls of tubes/caps from the acid bath, making sure to drain them, then add to DI bath.  Repeat this 2 more times with clean DI baths until the tubes have had 3 DI rinses.  Lay out tubes to dry.  Glass culture tubes can be placed in a tray in the food dehydrator to speed up the drying process.  

Notes for the Ann Arbor Lab regarding disposal of acid bath:  
1. Prepare 1 M NaOH by slowly adding 40 g of NaOH pellets to 1 L of water while stirring.  
2. Add a small amount of 1 M NaOH to the acid bath by dropper while stirring.  
3. Check periodically with pH paper until it has reached pH ~7.  
