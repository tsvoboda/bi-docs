# Sample Extraction

## 80% MeOH extraction for Supernatant, Polyphenol, and Antioxidant testing:

### Required Equipment

- 100% MeOH
- 1 L beaker
- DI Water
- Sonicator and Chiller
- 2 dispenser and storage bottles for 80% MeOH
- Shaker Table
- Centrifuge capable of reaching 3000 rpm
- Test tube rack and 40 15 mL test tubes

1. Prepare 80% MeOH by adding 800 mL of 100% MeOH to a large beaker and fill to the 1 L mark with DI water. Give it a good stir and ensure it is fully mixed.  Add this to two different bottles, one with a 5 mL bottle top dispenser and one with an 8 mL bottle top dispenser.

2. Remove the produce sample in the 15 mL tube from the freezer and thaw in the sonicator water (leave the device off during thawing) for 5-10 minutes or until the sample is sufficiently thawed.

3. Use the following table to determine the amount of MeOH to add to the samples based on type and sample weight.  

| Sample Type | Sample Weight (g) | Volume of 80% MeOH (mL) |
| ------ | ------ | ----- |
| Apple | 1 | 10 |
| Carrot | 5 | 5 | 
| Butternut Squash | 5 | 8 |
| Sweet Pepper | 1 | 5 |
| Spinach | 1 | 8 |
| Grape | 1 | 8 |
| Cherry Tomato | 1 | 8 |
| Lettuce | 2 | 8 |
| Leek | 2 | 8 |
| Potato | 4 | 8 |
| Zucchini | 4 | 8 |
| Squash | 4 | 8 |
| Wheat | 1 | 8 |
| Oat | 1 | 10 |
| Beet | 1 | 13 |
| Blueberry | 1 | 13 |
| Mustard | 1 | 13 |
| Chard | 1 | 13 |
| Bok Choy | 1 | 13 |
| Kale | 1 | 13 |
| Peaches | 1 | 8 |
| Pears | 2 | 8 |
| Nectarines | 2 | 8 |

4. Try to shake the sample so that it is all at the bottom of the tube and uncap, this is to make sure that any sample left higher up in the tube will not block the methanol from being added. Carefully add MeOH with the appropriate 5 or 8 mL bottle top dispensers. The tubes should be colored coded by sample type to make this process easier. To make up 13 mL of sample, simply add from both the 5 mL and 8 mL dispensers. A third 13 mL dispenser is not required.

5. Shake the samples by hand, turning upside down, and ensure that the produce pellet has fully thawed and is not stuck in the bottom of the tubes. It must be fully mixed with the MeOH or it will not extract properly, even with the sonicator and shaking table.

6. Sonicate the samples at the “Normal” setting for 1 hr, using the chiller to ensure the temperature doesn’t rise above 25 deg C during the sonication process. 

7. After sonication, give the tubes another good shake by hand.

8. Shake the samples on the shaker table for 1 hr at 150 rpm.

9. Centrifuge for 15 minutes at 3000 rpm.

10. Pour off the supernatant into a new, clean, 15 mL tube, using the cap to catch any stray produce matter that may not have centrifuged to the bottom. Put the original cap on the new tube to help keep track of your samples. Discard the pellet. 

11. Begin Supernatant, FRAP-antioxidant, and Polyphenol testing. The testing order for these three analytes is not important.  

12. Extractions should be stored in the freezer until ready for analysis if they cannot be tested on the same day.  Same day testing is preferred, if possible. **Helpful hint:** check your local regulations requiring your freezer’s rating for storing flammables. 

13. When ready for testing, thaw the sample if frozen, toggle the liquid in the container by tipping up and down gently. Do not shake or vortex to mix as this adds air bubbles which will affect the result. Ensure to pipette the sample from the middle of the vial. If you sample has a lot of particles, particulary starchy potatoes, it might be better to let it rest then take a sample from the middle of the supernatant.