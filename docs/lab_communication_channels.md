# Lab Communication Channels

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

This is a list of channels of communication, and recommendations for when and where to use each type of channel.  Within the BI Lab Network, we communicate within labs, between labs, to external collaborators, and to the general public.

_This isn't a prescriptive document, but should help labs and lab members easily find the right place to communicate!_

**Public discussions - [Forum](http://forum.goatech.org/c/real-food-campaign/9)**
- Anything you think the general public (even a little!) would benefit from seeing or contributing to.  
- Discussions about lab methodologies, definition of nutrition, best equipment to use.  General 'what', 'why', and 'how' questions.

**Inter-lab communications - BI Lab Slack Channel**
- Things which have internal importance, but little/no value in public discussion.  
- Questions, suggestions, concerns about the BI Lab SOP, Our Sci platforms, reimbursements, etc.  
- Discussions for lab members in other labs via direct message or channel posts.

