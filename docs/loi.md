# Total Carbon via Loss on Ignition (LOI)

### Required Equipment

There are two surveys for this SOP, **2020 RFC LOI pre-weights** and **2020 RFC LOI Final**. Each of these surveys can hold 36 samples, with 1 QC Sample and 35 LOI samples.

- Muffle Furnace
- Aluminum Baking Tray
- Large and Medium Aluminum Tins
- Coffee Scoop
- Crucibles and Racks
- Timer
- [LOI Manifest sheet to track samples](https://gitlab.com/our-sci/resources/-/blob/2be07b5f8f78d1e5cde2aee2ee3aad9f0bda7786/resources/soilLOIManifest.pdf)  

1. Each laboratory should conduct an audit of this process and determine which PPE is appropriate for their particular laboratories.  i.e. hot pads, heat proof gloves, apron, safety goggles, etc.

2. Ensure soil samples are fully air dry and sieved.

3. Using the coffee scoop provided, scoop a level amount out of the soil bag and place it in a large or medium aluminum weighing dish. This will give each sample a consistent volume that usually weighs 10-15 grams.  Alternatively, a large amount of soil can be placed in the tins and scooped out just before weighing.  

4. Place up to nine weighing dishes containing soil samples in an aluminum baking tray and dry for 24 hrs at 105 deg C. Usually, you can fit 6 large and 3 medium sized dishes into this tray. Ensure you orientate the tray so that it matches up with a manifest sheet and write the sample numbers and depths (if applicable) onto the manifest.

5. Place the empty crucibles in the dehydrator to ensure they are fully dry. 

6. Open survey **2020 RFC LOI pre-weights**.  Overview slide of the LOI process.

7. Enter Tray ID # (batch #)

8. Indicate whether or not the balance is connected to the app.

9. Enter QC ID#.

10. Remove a crucible from the dehydrator. Tare the balance and weigh the crucible.

11. Add the QC soil to the crucible and weigh. Do NOT tare the scale before adding the soil.  You want to record the total weight of the crucible + soil. The crucible weight will automatically be subtracted later when the software performs the calculations.

12. Enter Lab ID #.

13. Continue the process by weighing the crucible and then crucible+soil for each of the samples in rack #1. The rack holds 18 samples. 

14. If you are conducting testing with 2 racks of 18, continue on with the samples in the second rack.  Be sure that you match up the manifest with the samples appropriately.

15. Include any notes that may be relevant to this batch of testing.  For example, if you spilled a sample and it needs to be re-run, note that here.  Anything that might be worthy of note should be documented here.

16. Ensure you hit ‘Send’ when all of the measurements for this batch are complete.

17. Take all safety precautions into account when using a muffle furnace and avoid spilling any sample. 

18. Pre-heat the muffle furnace.  Extreme caution should be taken when using this equipment as it runs at tremendously hot temperatures.  Only trained personnel should conduct this part of the test.

19. Take care during this step. Using the rack holder, carefully and quickly place the samples in the furnace. Monitor the furnace temperature and start a three hour timer once the temperature reaches 650 deg C. It is best to continue to monitor the temperature until you are sure the temperature is consistent. If the temperatures rises up more than 670 deg C, the test will be invalidated and you will need to rerun the samples.

20. Once the three hour timer has concluded, immediately move the samples back into the 105 deg C oven  for at least one hour to cool down. Take great care in this step to ensure you are working safely. 

21. Open the survey **2020 RFC LOI Final**. Overview slide of the final LOI process.

22. Enter Tray ID.

23. Indicate whether or not the balance is connected to the app.

24. Similar to the LOI pre-weights survey, you will enter the QC ID and then the Lab ID in later steps.

25. Tare the balance and record the weight of the Crucible + soil, post combustion.

26. Calculate total carbon and send the survey when complete. The total carbon value should generally be <10%. If it is higher, it may be high in organic matter, or you might want to check your manifest to ensure you have measured the correct crucible. This is an easy error point. Also, make sure you tared the balance before taking a measurement.
