# RFC Lab Network Meetings Notes

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

Updated Meeting notes can be found [here](https://gitlab.com/our-sci/real-food-campaign/rfc-docs/-/wikis/Meeting-notes,-RFC-Lab-Network)

### September 13, 2021 meeting notes

**Lab Update**
- Ann Arbor - wet chem survey is mostly completed, just finishing testing and fleshing it out.  Should be available tomorrow. Still have small number of samples coming in.  We will be starting to use prediction models in our sample collection surveys for a few of the produce types that have enough data to have a good model (additional info under HW/SW, Process Issues).
- Chico - Started method development for eggplant (samples for MeOH ratio determination trial).  Awaiting wet chem survey.  XRF instrument inquiry started then paused to address power and space constraints.  Two of 3 soil sample collections have been processed through intake - one remaining from July.  Cindy is collecting at a partner farm today and we will have paired samples to receive and intake tomorrow.  One set of butternut samples has been cured and is in storage, intake scheduled for October.  A second butternut set is expected from today's collection.
- France - little to update at this point.  Olivia not yet translated the surveys, international exhibition for Valorex has taken up a good deal of time.

**CRARS update**
- Christina Pease has joined CRARS as our new research project manager
- Seth will be relocating at the end of October.  Possibility to stay on with the Center to work remotely, or train to be a technical service provider for carbon farm planning - TBD 

**Hardware/Software, Process Issues**
- Certain produce types will have the option of running scans to predict values of analytes.  Integration could be in intake or collection survey – need to determine where best to locate since there is value in having multiple scans (i.e. analyte differences in point of collection versus intake, difference from one bionutrient meter to another, etc.).  Dan to check with Emily on state of development, should be done this week.  Already have hundreds of predictions from users taking produce scans in the field.  Lab analyses will help validate predictions.
- Respiration method still being updated, but in final stages.  Properly venting meter is currently the challenge, but many process improvements over previous method.  

**Next meeting**
- September 27, 2021 at 8am PDT/11am EDT

### August 23, 2021 meeting notes

**Lab Update**
- Ann Arbor - working on Peach, nectarine, and pear method development.  Rescanning 2020 soils with new device calibration. Running intake, Moisture content, LOI on samples.  Still waiting for respiration and wet chem.  Wet chem scripts are expected by Wednesday and then a day or two to complete the survey and test. Should be available by end of the week.
- Chico - Completed collection of tomatoes and butternut squash at Scott Park’s farm on 8/18.  Intake of tomatoes is complete; butternut intake to occur mid-October after curing and storage period to optimize nutrient levels.  Soil intake and scans are proceeding.  Method development for eggplant is forthcoming.

**BI-RFC updates**

**CRARS update**

**Hardware/Software, Process Issues**
- discuss issue with toggling/mixing samples that have high starch content or small particles as they can interfere with the wet chem results.
- slack channel for reporting bugs and software issues?
- XRF data update
- Data dashboard update
- Chico is looking to acquire an XRF instrument; can Ann Arbor provide info on their instrument?

**Next meeting**
- Currently scheduled for Labor Day Holiday (US) Sept 6, 2021 - reschedule to?


### August 9, 2021 meeting notes
_Katie, Priya, Seth, Devin_

**Lab Updates**
- Ann Arbor - Not a lot of samples coming in to the lab yet although many packets have gone out.  Finalizing LOI survey and it will be ready to use this week.  Chico plans to send samples for LOI intermittently rather than at the end of the season.  Katie discussed issue with mixing wet chem samples that are very starchy or have lots of small particles.  To discuss next week.
- Chico - Chico will investigate method development for eggplant. Wondering where things are at with XRF data, Wet Chem survey, new/old Respiration methods, Dashboard.  Wet chem survey is almost done.  Katie could not give any additional updates on respiration or the dashboard but will ask Dan and Greg.  XRF is waiting for Greg to calibrate data.

No additional updates discussed.  

### July 26, 2021 meeting notes
_Aundrea, Dan, Priya_

**Lab Updates**
- Ann Arbor - Contact Barbara for lab and method development questions while Katie is away (returning Aug 9)
- Chico – ongoing intake of Full Belly farm tomatoes and zucchini. Upcoming sampling trips (Foster and Park) end of July and mid-August respectively.  Still paused after extraction step.  Respiration is also still paused.  Cindy inquired about method development (MD) for eggplant— Chico to move forward with this sooner than nut MD given chemistry challenges.  Aundrea to contact Victoria again for Greg Reese’s samples.

**BI-RFC updates**
- BFA requested redesigns of some of this year’s surveys to better meet expectations 
- New Zealand group interested in assisting with publication(s) of RFC methodologies

**CRARS update**
- Priya confirmed that eggplant will be next produce type for which Chico will develop a method 
- Discussed having a meeting regarding nutmeat method development. Proposed participants are Garrett Liles, Cindy Daley, Maria Giovanni, plus Aundrea and SRAs

**Hardware/Software, Process Issues**
- Respiration – the legacy app quit working on all Chico and Ann Arbor tablets.  Ann Arbor has one chromebook that can still run the app.  Discussed having a surveystack survey for running the sensor without having scripts for calculations – Chico has a spreadsheet/dataform for calculations that could be modified for this.  Aundrea requested tank check/QC integration into survey.
- wet chem calculations – Chico requested calculations to convert polyphenol and AOX values adjusted for fresh weight concentrations.  Data explorer tool should allow a producer to download their data (in csv) with final values, coming soon.
- confirmed soil intake survey is titled “BI 2021 – soil intake”
- Seth to add members to Chico FarmOS so that other lab members can access

**Next meeting**
- August 9, 2021 --> Aundrea will be away, Seth to attend


### July 12, 2021 meeting notes
_Aundrea, Dan, Seth, Priya, Constantin_

**Lab Updates**
- Ann Arbor – Samples Slowly trickling in.
- Chico – Paused on 2021 wet chem and 2020 respiration under the Chico testing lab organization.  All 2021 amaranth samples are extracted and awaiting wet chem testing; paired soils from this harvest have had intake survey completed (outdated survey, results to be moved to updated survey).  Sampling event at Full Belly Farm on the schedule for July 20. Samples from Greg Reese could be received for intake at the same time (contact Victoria). Almond method development paused but likely to resume this week.

**Hardware/Software, Process Issues**
- Legacy App - 2020 updated baseline respiration not functioning as expected (no curve displayed after running measurement). Dan added the updated respiration survey to the online form for the Chico testing lab group, but Chico is experiencing the same issue that is occurring in RFC lab.  Try different android devices to see if they work, possibility to have a script written for surveystack if legacy app does not work – not preferred.
- From now on use this soil intake survey: https://app.surveystack.io/surveys/60e202358682260001751f56
- update on the 2021 surveys (wet chem, LOI) – Katie recommended some modifications before Chico moves forward with 2021 samples
- Chico to use new sample collection survey (pinned) for Full Belly Farm; research collection survey on pause but will be updated for Fall.  If Chico has short-term notice for research collection, then give Dan a heads up asap.
- retitle surveys for 2021 – Future discussion with Katie 
- SOP updates for 2021 - Incorporated into surveystack under documentation (left side bar)

**Next meeting**
- July 26, 2021 at 8am PDT/11am EDT

### June 21, 2021 meeting notes
_Katie, Benoit-Pierre, Seth, Aundrea, Priya, Dan_

**Lab Updates**
- Ann Arbor – Couple of issues with intake over the past 2 weeks, so working on process improvements
- Chico – Paused on wet chem, respiration ongoing.  Focusing on 2020 paired soils from Five Points, OVP peppers, and CIG brassicas. Aiming to have soils shipped for LOI and XRF by early next month.  All 2021 amaranth samples are extracted and awaiting wet chem testing; paired soils from this harvest are awaiting intake.  Need to contact Si-Ware for a connection issue with the full spec.  All Chico’s loaned reflectometers (four 2019 models, three 2020 models, and one unique model, for a total of 8 devices) plus a chromebook were shipped to Nat at Our Sci on June 16; awaiting receipt of replacements.  Will be coordinating with Greg Reese to receive samples once replacement reflectometers have arrived.  Started research into almond method development.
- France – No updates to report, little progress in the last couple of weeks.  Anticipating start of receipt of samples in August (potatoes, carrots, other crops later in summer)

**RFC updates**
- Launch of the Bionutrient Institute https://www.bionutrientinstitute.org/
- Successful models for 12 different crops using the bionutrient meter; participants will scan to develop predictions and then some subset of samples will be tested by the lab.  Further refinements to models may be available by fall
- Releasing the data explorer: https://app.surveystack.io/static/pages/inner?explained=polyphenols
- 2020 report coming soon
- Another conference session was held recently (previous sessions were April and June)
- Greg to schedule a meeting with Cindy Daley and Dan Kittredge soon for discussing 2021 work plan/sample goals

**CRARS updates**
- Over the weekend the Center submitted grant for almond and walnut ND (North State Hulling); will wait to hear back

**Hardware/Software, Process Issues**
- Almond method development: Chico would like to pick Katie’s brain about wet chemistry for lipids and omega 3/6 quantification. Link to outline/Q&A document: https://docs.google.com/document/d/1DpWr_lmnse0gYkSBGkNQEHMNGJqqRAmveMbFWm710nE/edit.
Tentatively schedule for Thursday at 8am, Aundrea to send a Zoom link.
- Data question: How do we get XRF data in a format that we can manipulate?  2019 data for XRF is unique, Our Sci is creating a pipeline for reporting – coming soon.  Revisit at upcoming meeting if not sooner.
- update on the 2021 surveys (wet chem, LOI): density and wet chem scripts not yet complete.  Released several new survey features that may need adjusting, TBD.  If strange behavior is observed in surveys, please report for troubleshooting/fixes.
- In 2021 surveys, new dropdown menu for batch ID so that only existing batch IDs can be selected.  Intake survey will reference receiving survey, and receiving survey will reference the collection survey.  If no sample collection survey, then an alternate method will be available to enter receiving.
- New respiration method development well underway, will come with its own incubator at 30 C.  Chico with option to continue using BOD incubator and set to 30 C to match the rest of the respiration data submitted in surveys.

**Next meeting**
- Protein and lipid wet chem discussion for almond nutrient density, Thursday at 8am PDT/11am EDT 


### June 7, 2021 meeting notes
_Aundrea, Priya, Seth, Dan, Benoit-Pierre_

**Lab Updates**
- Ann Arbor – Finishing last parts of survey development.  Octavio performing model creation for bionutrient meter.  QC checks coming soon.
- Chico – Paused on wet chem, respiration has resumed.  Respiration data needs QA review (2020 soils) – Chico to check from Mar 10 onward to correct Lab ID for samples that were rerun.  All amaranth samples are extracted and awaiting wet chem testing.  Need to contact Greg Reese and discuss workload/sample volume and timeframe – Dan to connect Aundrea with Victoria and Emma for discussing Greg’s samples. 
  - Is there a Yearly Plan for 2021 with production sample goals/method development goals defined for partner labs?  See RFC update notes
  - Literature search for almond method development started last week.
- France – All farm partners have been contacted and lab has estimate of samples to expect.  Supplies to be ordered.  Samples expected to arrive in August.

**RFC updates**
- No plan to add crops this year. Push to bring on larger organizational partnerships, their goals could set development of methods for additional crops. Bionutrient Institute launching July 1, RFC Lab under this umbrella.  Boston Lab setting up, greater analytical capability than RFC Lab. Greg and Dan to discuss 2021 RFC analytical package, moving to farmer/participant-funded. Onboarding farmers in the US for 2021 production season.

**CRARS updates**
- CIG plot could produce potatoes for ND, corn also under consideration but planting design is complicating factor.  Request submitted for no-cost extension to continue till/no till and BEAM/no BEAM comparisons.

**Hardware/Software, Process Issues**
- New 2021 surveys – internal testing to come.  Features to make the surveys more useful scheduled for June 15. 
- Re: translating the surveys to French, would be helpful to be able to see all questions at once.  Dan to show Olivia how to use the surveystack builder so that questions will be listed.

**Next meeting**
- June 21, 2021, at 8am PDT/11am EDT

### May 17, 2021 meeting notes
_Priya, Katie, Dan, Benoit-Pierre, Aundrea_

**Lab Updates**
- Ann Arbor - getting lab ready for new season. Videos created, need review if possible. To be uploaded to YouTube and linked on surveys. New label printer ordered, rolls instead of sheets.  All surveys, except Respiration should be ready by next week.  Reviewing process for shipping samples from France.
- Chico – Pause on respiration and wet chem.  All chard and spinach samples are extracted and awaiting wet chem testing.  Awaiting new respiration sensor from Ann Arbor and rest of 2021 surveys, to be delivered by May 19.  Beets harvest occurring today May 17.  Costech 8020 elemental analyzer now operational.  Option to conduct comparison testing of LOI and C:N analysis.
- France – Farm partners have been contacted and awaiting responses.  Benoit-Pierre presenting data to partners.  Arranging for and estimating number of samples for this coming season.  Would like a report for each participant.  Dan acknowledged request for reporting, last year was spreadsheet with read-me; to come: Data coffee shop (?) and dashboard.

**RFC updates**
- First prototype of new respiration device
- Running models for all crops from 2020 season.  Identify crops for which predictive capacity of reflectometers works, will help determine which crops to focus on this season.
- Bionutrient Institute to launch sometime after June 1.

**CRARS updates**
- South Dakota still has frozen soil so nothing planted yet, harvest expected late fall.
- COR performing their own nutrient analyses. 
- Almond Board collaboration, method development on horizon.  Complete amaranth analyses first then conduct research into analyzing nutmeats.  Funding for project still a ways out, but will be ready begin testing once it is secured. 
 
**Hardware/Software, Process Issues**
- Greg Reese to send Chico samples for 2021 production season, Aundrea to contact.  Confirm interest in assessing ND differences with shipping time as variable – would send same samples to Chico and Ann Arbor, therefore would only need to send one paired soil.
- Aundrea/Seth to confirm that reflectometer is working on all tablets (soil tablet not yet done)
- Update for XRF methods: Wash greens in particular -- or anything visibly dirty -- to remove residual soil particles so that produce XRF is not contaminated with soil.  Leeks are the most difficult because of their layered structure, can hold both residual particulates and excess moisture.  Wash recommended during receiving: place washed produce in a new clean bag, and roll up original sample bag and add to new bag along with clean produce.  Chico likely to wash with intake, at least for the current round of beets intake.
- Extraction ratios – separate meeting to be scheduled, tentatively 8am PDT/11am PDT at Thursday.  Katie has identified the need for initial dilutions for several produce types.  https://docs.google.com/spreadsheets/d/1D-GWFiCFtZSoy33291pGawslncDLb9aqh8T6N2Vepig/edit?usp=sharing

**Next meeting**
- (tentative) Discuss/review extraction ratios: May 20 at 8am PDT/11am EDT

### May 3, 2021 meeting notes
_Priya, Benoit-Pierre, Katie, Aundrea, Dan_

**Lab Updates**
- Ann Arbor – Working on surveys.  Intake survey is done, but buggy with regard to reflectometer, some connection issues with legacy software.  Sample receiving is going to be modified so that it includes an additional feature to verify collection survey was created.  Moisture content modifications coming soon (tomorrow).  Katie reviewing extraction ratios and dilution factors, summary document to be provided.  Considering changes to initial dilutions (e.g. 5x instead of 10x for polyphenols).  New wet chem survey will make results data easier to archive when it is rejected/samples require a rerun.  
- Chico – Pause on respiration and wet chem.  Awaiting new respiration sensor from Ann Arbor and 2021 surveys.  pH electrode was replaced and testing will resume.  Spinach harvested April 26 & 27 – no reflectometer scans, chard harvest May 3, then beets May 10.  Maria Giovanni to visit while chard intake is ongoing.
- France – All 2020 analyses are complete.  Coordinating with farm partners for 2021.  Getting organized for the next campaign. 

**RFC updates**
- Ann Arbor reaching out to farm partners now.  No updates for bionutrient institute yet.  This year, planning to have nested group structure for admin controls in SurveyStack. That way, technician activities can be reviewed and corrected if needed, and group data are better isolated from each other to protect farmer/research interests, and select people can be granted admin permissions for their group.

**CRARS updates**
- Circle back to COR meeting update for nutrient density goals this year.  No follow up meeting yet.  

**Hardware/Software, Process Issues**
- Respiration/Chico sensor update: Greg still working on it, Katie checking-in for Chico.  New respiration method coming in June.
- 2021 wet chem and range calibration surveys on SurveyStack?  Wet chem survey built, scripts to be added (Greg).  Survey will include a prompt for device calibration after 12 hours.  Calibration of reflectometers will still be completed in the legacy software.
- Intake survey use for today?  Intake survey is on version 23 – new survey needed, fresh/version 1 so that it doesn’t get too big and cause unnecessary lagging -- to be turned around quickly
- Will we continue to use the Chrome/web-based version of SurveyStack?  Or will we be able to use the app at some point? Confusion over which apps to use:  If have internet access, then either is fine.  But if no internet access, then use app.  Greg recommends use of chrome as default. 

### April 19, 2021 meeting notes
_Katie, Benoit-Pierre, Dan, Aundrea, Seth_

**Lab Updates**
- Ann Arbor 
  - new sensor for Chico has installation issue, Greg needs to review as the technician had trouble installing. Ann Arbor has replaced their sensor as well.  New respiration sensor design to be ready in June; survey to come thereafter.  
  - Chico LOI soils are mostly completed
  - Intake survey is ready for review.  Still working on hardware connections and adding in photos and videos. All other surveys for 2021 are expected to have completed drafts/designs this week.  
https://app.surveystack.io/surveys/605b5f11ab0f6c0001cd9607
  - LOI surveys are ready for review, still working on calcs and hardware for weights.
https://app.surveystack.io/surveys/6074588d7794490001831f56
https://app.surveystack.io/surveys/6075f6fe30a4a40001bd141a
  - Katie can go over Intake and LOI surveys at the end of meeting if time allows
  - Reflectometer – clunky process, script in SurveyStack goes back to OurSci app to run the measurement
- Chico 
  - Pause on pH/EC and respiration.  Awaiting new respiration sensor from Ann Arbor.  
  - Continuing problems with pH electrode; a replacement is being shipped to us.  Method realignment to come.  
  - Amaranth harvest on the horizon; spinach to be harvested April 26, chard to follow May 3, then beets May 10.  
  - Maria Giovanni to have students working with RAD-Lab to run samples during the summer; Aundrea to discuss sample design/load with Maria TBD.
- France – Finished all the analyses for 2020.  Coordinating with farm partners for 2021.  Getting organized for the next campaign. 

**RFC updates**
- 1st of several RFC webinars: high level outcomes for 2020, and directions for 2021
- 2021 composite sampling; Dan working on updating planting forms.  At some point should review if current planting form works for Chico research.

**Hardware/Software, Process Issues**
- 2020 versus 2021 surveys for Amaranth harvest?  Ideal to use the 2021 software and new forms.  A few things need to be resolved before the new software is ready.  New intake survey currently accommodates 1 scan.  Could skip other reflectometer scans but issue still needs to be addressed for all wet chem survey scanning.  Chico - Likely course of action is to process intake and stop at freezing step, then extract and run chemistry when surveys are ready
- If 2021, then compositing produce replicates: Participants will enter the number of produce composited for the sample.  For Chico, could do all four produce in treatment plot as a composite, but would only be able to represent 3 scans.  Important thing is that one collection survey represents a single composite sample, regardless of # of produce composited.  
- Intake and LOI survey review, Katie recommends having someone go through the survey and test functionality.  Chico to start with Cooper today, and then all SRAs this week.

**Next meeting**
- Circle back to COR meeting update for nutrient density goals this year.


### April 5, 2021 meeting notes
_Katie, Seth, Aundrea, Dan, Priya_

**Lab Updates**
- Ann Arbor – Katie continuing to create surveys, goal to be completed this month.  Intake survey in progress, aiming to be done with 1st draft this week.
- Chico – Pause on pH/EC and respiration.  For pH/EC, meters/electrodes readings are varying based on mass:volume amounts, need to reassess approach to align with methods for lab certification and improve accuracy and consistency.  For respiration, QC soil and tank values have trended upwards and the last batch analyzed all samples were “too high”.

**RFC updates**
- Meeting with BFA, new lab setting up in Massachusetts. Rebranding RFC to come, Bionutrient Institute. Separate silos: policy silo to better define nutrient density, and lab and data analysis silo.  Goal to have soft launch in June after recruiting of new participants is complete.

**CRARS**
- Working with tribal college in SD, short growing season. Cover crop termination trial producing soil health and produce nutrient density samples in late summer/early fall.
- Circle back to COR meeting update for nutrient density goals this year.

**Hardware/Software, Process Issues**
- Respiration: Katie has completed stability testing of sensor.  Will send another sensor to Chico to compare new sensor with existing sensor.  Chico existing sensor to be named CO2-3a; run baseline testing  using existing and new sensor and report back.
- pH strip versus electrode readings, pH method 1:1 versus 1:2 soil to water ratios.  Dan – considering eliminating the pH strip testing; Logan Labs to be subcontracted to run other testing, pH would be a component.  Katie suggests reviewing EPAs method for testing soil pH.  
- Reminder: 2019 Long Beach soils (November only) – no XRF or LOI in data set.  Katie to check on these samples and rerun samples if necessary.  

**Next meeting**
- RFC update from BFA meeting
- CRARS update from COR meeting

### March 22, 2021 meeting notes
_Katie, Aundrea, Seth, Dan_

**Lab Updates**
- Ann Arbor – Working on including new process for combining three samples to one. Restarted respiration and conducting stability testing. Creating pH survey. Potential to replace Chico’s sensor given drift is evident.  
- Chico – Respiration proceeding for production samples (mizuna needs sample reruns, Five Points in progress, need peppers 4-8 soils, all mustard soils still to do too).  Measuring pH and then will send soil for XRF and LOI.  Soil-only projects also with a backlog of samples for respiration.
- France – Analyzed a series of polyphenol tests (50 samples), anticipates completion of all analyses before next meeting.  Also pH analyses are complete, but still need to be registered (Benoit-Pierre, can you clarify? Do you mean submitted in the survey?)

**Hardware/Software, Process Issues**
- Discuss collection surveys, which is more appropriate normal collection vs. research data collection, which uses replication – relates to conversation on compositing samples.  Chico anticipates high number of replicated samplings with CIG-Till v. No-till study (aka. University Farm grown and harvested crops) for current (amaranth) and following (solanaceous) rotations.  Uncertain whether farmer/collaborators would have replicated sampling designs – could be like Five Points butternut where opportunity presents without much notice.
- Aundrea/Seth would need to propose change to composite sampling design for this year’s CIG rotations and get PI buy-in; plan to approach Cindy after "This Way to Sustainability" Conference (March 25 & 26). 
- Dan proposed keeping Surveystack sample collection survey the same or with a few updates for highly-replicated sampling design.  Katie proposed having a tick-box to indicate individual versus composite samples – adds complexity to the survey, but would cover instances where participants send only one sample.
- 2019 Long Beach soils (November only) – no XRF or LOI in data set.  Katie to check on these samples and rerun samples if necessary.

**Next meeting**
- Meeting to review composite sampling process Tuesday, March 30, 8-10am PDT/11-1pm EDT

### March 8, 2021 meeting notes
_Katie, Dan, Seth, Aundrea, Benoit-Pierre, Priya_

**Lab Updates**
- Ann Arbor – Continuing merging data, working on 2021 surveys, naming data types is an issue to work out -- need universal naming -- so that order can be maintained.  Goal is to only have one survey for all produce types.  Surveys will include photos; videos may come later.  Respiration test is having issues, see below.
- Chico – Focusing on soils backlog and respiration, having issues with respiration as well.  Amaranth planting last week ahead of wet weather this week.
- France – 30-40 soil samples to scan with full spec.  Shaker table has arrived and polyphenols can be analyzed.  
  - Question about standard curve: To wait or proceed with samples while curve is shaking?  Katie suggests waiting for the first curve to confirm it works, then proceed, but can start curve and proceed with samples, shaking concurrently, when confident that curve will be accurate.
  - Six to 7 European farm partners interested in participating in RFC.  Benoit-Pierre requests scheduling a meeting to organize for 2021 production season/next campaign.  Benoit-Pierre to coordinate with partners for meeting and sample collection.  Schedule meeting with Emma who manages US farm partners to ensure training.

**RFC update**
- Winter work proceeding.  Goal for next season is to return results 3-6 weeks.

**CRARS update**
- Amaranth seedlings were transplanted last week ahead of rain this week timing is late May/early June harvest.  
- Meeting with California Olive Ranch (COR) this week to discuss nutrient density for olives/olive oil – Priya to report back on industry research interests.
- Majority of planned RFC samples will be from researchers (CIG, side by side management comparisons), a tribal university in South Dakota, and could have more Long Beach samples.  Farm OS data from research would be collected/managed by Seth.

**Hardware/Software, Process Issues**
- Respiration – calibration and ambient values are reading higher than previously.  Ann Arbor experiencing similar issues that started over the weekend.  Calibration gases measuring higher than certified ppm.  Is flow rate an issue (0.5 LPM vs. 1.0 LPM regulators)?  Pressure (i.e. how fast can you push a bolus of air)?  Dan notes that faster is better.  Is sensor robust enough to handle jostling?  Stress-testing to come.  Incorporate and continue comparison tests, inside/outside ambient, calibration gases.  
- 2021 respiration and survey to be available June 1, but relies on the same sensor at this time.

**Next meeting**
- Discuss collection surveys, which is more appropriate? normal collection vs. research data collection which uses replication

### February 22, 2021 meeting notes 

_Katie, Dan, Priya, Benoit-Pierre, Seth, Aundrea_ 

**Lab Updates** 
- Ann Arbor – Respiration is ready, cross check is done.  Respiration surveys are updated.  Calibration gas measurements should now be incorporated in the protocol and data can logged into the Respiration QC data spreadsheet
  - Respiration data should be added here: https://docs.google.com/spreadsheets/d/1WTczgIaHXMkQq29CT36U0LASrpBd9pBHJQd9shNhRGw/edit?usp=sharing   
  - Cross Check data should be added here: https://docs.google.com/spreadsheets/d/1gl9gdG8ATZGSvDZZNw0aZ9Uk6Tv7TI671ZrqQnoNlyw/edit?usp=sharing 
- Chico – Need to rerun polyphenols for potato cross check samples (likely Tuesday), working on backlog of soils and would like to run 2020 respiration when ready.  Aundrea/Seth to review respiration surveys and provide feedback before Wednesday. 
- France – Supernatant and pH on the to-do list.  Shaker table is still delayed.  Q: Full spec scans – how to send? Google drive or BOX are options to upload.  Aundrea to provide feedback when Chico uploads scan data. 

**RFC update**
- Almost all 2020 data merged, then share with farmers/participants 
- Link to 2021 planning document: https://gitlab.com/groups/our-sci/real-food-campaign/-/epics/2 
- Major goal to have close to real-time feedback. RFC won’t be making crop additions this year; but will add crops as institutions request and fund development of methods. 
- As a side project to 2021 plan, goal to compile literature review work and share across labs so that an individual’s effort does not have to be repeated. 

**CRARS update**
- Amaranths seedlings growing slowly, to be transplanted week of March 8 for projected harvest in early June.  
- California Olive Ranch interested in nutrient density -- COR to fund a post doc position; Priya in process of posting job listing, anticipate filling the position ~2 months. RAD-Lab potentially to take on work to investigate existing methods of analysis for olives and olive oil.  Ideally olive method development could be completed this year.  Existing COR RA project is a California Healthy Soils demonstration project comparing cover crops vs no cover crops.  
- Collaboration with North State Hulling, phase 1 initiative to transition to regenerative practices, biochar amendment using almond shells/hulls.  

**Hardware/Software, Process Issues**
- Chico to send full spec scans from dedicated laptop the week of the 22nd – Aundrea to follow up 
- Respiration survey – Katie, Dan and Aundrea met to review QC soil data.  Katie observed decline in carbon over last month, possibly due to residual moisture in soil?  Katie will dry a subsample of lab QC soil and test oven-dried vs. Air-dried to compare values over time.  

### February 8, 2021 meeting notes 

_Dan, Katie, Benoit-Pierre, Seth, Priya, Aundrea_ 

**Lab Updates** 
- Ann Arbor – working on merging/processing 2020 data. Working on wetchem studies to determine QA/QC viability/dilutions/repeatability. Taking longer than anticipated.  Wetchem QC discussions ongoing, Greg developed an approach, more info to come 
- Chico – Produce analyses completed, data under review.  Soil processing done, pH and respiration to do before sending for LOI and XRF 
- France – question from Colleen – when will data be available? Working on merging the data but still a few weeks out. A CSV can be made in the meantime for her to manipulate.  Still waiting for shaker table... Soil analysis ongoing, plus supernatant 

**RFC update**
- Grantham funding secured; RFC in the process of planning for 2021.  Revisions to sampling (i.e. compositing produce samples), new crops/produce types will be added in response to groups that want to work with RFC, e.g. soybeans proposed by a group and/or CRARS interested in olives, almonds, and walnuts.  Tree nuts would likely require proteins, discussed further under process issues. 

**CRARS update**
- Heinz grant proposal (tomatoes and compost tea) in process, submittal deadline approaching.  Amaranths are seeded to be transplanted in about a month for harvest in mid-May/early June. 

**Hardware/Software, Process Issues**
- Tree nuts and olives: 
  - Current wet chem method to analyze grains for proteins takes a long time, methanol extraction takes 4 hours, plus sodium hydroxide extraction takes another 2 hours, process is very time intensive requiring 13 hours/2 workshifts to complete. 
  - Olives and nuts would require fatty acid/oils(?) analyses which would require investment in method development.  New CRARS grad students could take this on if interested. 
  - France uses NIR to predict protein in wheat or oat – something for future discussion.  Chico to acquire an FTIR... could NIR method be adapted for that?  Would RFC still want wet chem data for reflectometer development?  TBD... 

- Migration of surveys from OurSci app to SurveyStack – Chico seeded amaranth crops (spinach, chard, beets) for estimated harvest date May/June 2021, as relates to 2021 production season readiness.  Have a hardware integration/working prototype – reflectometer can connect to SurveyStack; expect early March for migrating surveys and then additional time to test before harvest.  One advantage will be to have a single produce intake survey instead of separate greens and fruits, roots and tubers surveys.  One tool possibly available by May: ability to correct for Lab and Batch IDs mis-entries in real time.  

- QA/QC topics: Aundrea to share intake survey outline/feedback.  Regarding reanalysis of acceptable wet chem values.  To fix in the DB, go into the survey and modify Lab ID so that software ignores bad data, I.e., change “12-XXX” to “12-XXXerror" 

- Cross check program, # of samples and timeframe? Leafy greens & root veg (4 types likely), triplicate of each type, labs to run on the same day. Katie to ship today, shipping by UPS 

- Uploading full spec scans? Chico would like to send/upload and delete from dedicated laptop to free up hard disk space (25 GB capacity drive), refer question to Greg – Katie to message  

- Respiration survey – Katie to discuss with Dan and Greg.  Chico tested calibration gas and sensor read high.  Sensors are good at reading relative values, not perfect at detecting accurate values.  Since surveys are measuring change from baseline day to final day, then results are acceptable 

**Next Meeting and To-Do List…**
- (aundrea) share intake survey outline/feedback
- (katie) send cross check samples

### January 25, 2021 meeting notes 
_Dan, Seth, Katie, Aundrea, Priya_ 

**Lab Updates** 

- Ann Arbor – working on the wet chem for apples (MD). Respiration – QC values are declining.  Sensor maintenance may be needed.  Calibration gas standard will be checked again using continuous flow.  Calc issues need to be resolved too.   
- Chico – Sieving and scanning soils collected with Brassicas.  Wet chem for Five Points butternut is nearly complete, data needs QA review, to be addressed this week. 
- France – completing the AOX analysis this week, one dilution to do.  Awaiting a new shaker table to do the polyphenol and pH analyses. 

**RFC update** 

- Grantham Foundation grant has been signed and confirmed. 

**CRARS update** 

- ARI grant-writing for processing tomatoes in progress. Heinz is interested in Regen Ag, compost and compost tea applications.  Cherries tomatoes were not analyzed in 2020 b/c of quality issues (shipping crushed and/or rotten samples).  One group took on hydroponic vs non-hydroponic tomatoes – shipped in egg containers.  RAD-Lab to check into method to harvest and ship tomatoes from Heinz field to lab.   

**Hardware/Software, Process Issues** 

- Migrate surveys from OurSci to SurveyStack – more flexibility in logic and survey construction, will improve process, and be easier to QA/QC.  Working on connecting instruments to SurveyStack, UX. 
- Building a better set of QA/QC tools – improve error checking.  Consider what tools we want/need for meeting tomorrow.  Meetings to generate a set of tools for Octavio to create.  Dan/Greg to mock up survey environment.  Slowness of database is attributed to client-side loading and running, not server-side.  Recognized as a problem but uncertain whether it can be resolved during this year’s winter work. 
- Wet chem values triggering sample/batch reruns?  – still needs review; option to run one standard with batch as another QC check 
- Cross check program, # of samples and timeframe? Leafy greens & root veg (4 types likely), triplicate of each type, labs to run on the same day.  Overnight shipping and intake processing planned for Thursday next week. 
- New butternut and apples survey – density measurement is done first, RAD-Lab to review survey and provide feedback.  Katie would like to run butternut this week.   

**Next Meeting** 

- QA/QC discussion for developing new tools for 2021 production season, at 800 hrs PST/1100 hrs EST on Jan 26, 2021

### January 11, 2021 meeting notes 
_Katie, Dan, Benoit-Pierre, Seth, Aundrea_ 

 **Lab Updates** 
- Ann Arbor – Issues with respiration method, don’t use until notified otherwise.   
- Chico – Working on Five Points butternut squashes.  Limited work capacity while half of lab staff is quarantined. 
- France – finished LOI last week, problem with shaker table limiting capacity to complete pH, polyphenols.  XRF for shipping, packaging will need to comply with customs.

**RFC update** 
- Waiting on the Grantham grant, funding expected any day now. Funding will impact winter work: How to make RFC self-sustaining?  Considering fee-for-service business model.  Change(s) in operations, for example: 3 carrots from 1 field = 3 samples --> 3 carrots from one field composited into a single sample, reduces cost to farmer.
- Adding apples to produce types; industry partners to drive additions of other produce types.  
- Data: aiming for at least a biweekly review period or as close to real-time as possible. 

**Hardware/Software, Process Issues** 
- Instrumentation and data output integration with regard to a lab information management system (LIMS) 
- For analyses with manual data entry: have separate survey in SurveyStack per analysis, create in SurveyStack.io  -Aundrea to review 
- OpenTeam: library of questions for SurveyStack questionnaires 
- Tool: Upload exported csv from instrument, coming soon -- check back in March 
- LIMS – Mason to take it on this week; should have separate meeting, circle back at the next meeting 
- Regarding data review/quality assurance (QA): Katie goes batch by batch with her data review – separate discussion for next Tuesday's meeting 
- Recommends for wet chem: check dashboard, check each individual survey before discarding sample.  For soil: make sure all calculations work.   
- Organic matter causing some issues, High OM labeled unless unclear, have to confirm by directly checking the soil. 
- Aiming to do more quality checks -- focus of winter work; consider how to improve the quality checks
 
**Next Meeting and To-Do List…** 
- "Review of quality check" meeting, at 8:00 PST/11:00 EST on Jan 19, 2021  
 
### November 30, 2020 meeting notes
_Katie, Aundrea, Benoit-Pierre, Seth, Dan_

**Lab Updates**
- Ann Arbor – Expected to meet goal for total number of samples for the year.  Schedule cross-check for fresh veg the week of Dec 14; confirm at next check-in meeting.  Probably leafy greens and root vegetables.  Katie to provide Lab ID labels, intake as normal for production.  Customs requirements still need to be assessed for France. France to provide potatoes and carrots because of anticipated shipping duration.
- Chico – Mizuna and mustard harvest and intake, plus butternut MD. Next steps after developing curve? Share process – take photos, share data.
- France – continue to intake carrots, still need to do soil and wet chem.  pH strips and meters are showing a difference.  Need to work out issues with wet chem.

**Hardware/Software, Process Issues**
- Sample traceability, Chico to model its LIMS after RFC process for non-RFC analyses
- Is a cloud-based system on the horizon?  
Plan is to connect hardware to SurveyStack platform so that could edit data anywhere, and retire oursci kit app.  CSVs could be uploaded to surveystack and have a dashboard that merges data together.  Would require some consulting time if needed in the near future, but would be a goal over the next year.  Katie will be doing research into LIMS for cost comparisons.  Grantham grant would fund instruments for integration into surveystack so Our Sci would be doing a similar effort to what RAD-Lab would be doing for newly acquired instruments.
- Wet chem for greens/produce needing dilutions – any way to streamline process?  Can dilute right away, would need to QA for low concentrations and then re-run at standard.
- Link for ordering cuvettes, would like another 40 or could buy a dehydrator/nail dryer. Cleaning protocol reminder: Rinse with tap x3, rinse with DI x3; soak in hydrogen peroxide periodically.

**Next Meeting and To-Do List…**
- Aundrea: Share data output types from instruments and analyses + format for integration
- Aundrea: Share butternut intake process

### November 2, 2020 meeting notes
_Katie, Dan, Benoit-Pierre, Seth, Aundrea, Priya, Cindy, Greg_

**Lab Updates**
- Ann Arbor – Limited in new sample intake, mostly grains.  Wet chem is getting close to completion.  Soils may be sent out.  Potentially partnering with other soil lab (Logan Lab) and/or agronomists to get more value from data.  Goal would be to enable more self-sustaining collaborations.  Could have additional workflow to have another lab or agronomist review.  Would occur if we were receiving samples from partner farmers.
- Chico – Butternut research continues, MD proceeding.  Grating + grinding for juice only.  Grating evaluated for wet chem, grating + grinding not compared (found to be a bit too wet).  Mass to MeOH ratio is not yet selected.  Half of butternut samples collected and stored at the SPA (day-time temps warm but not too hot), awaiting intake.  
- France – Lab work winding down, perhaps 2-3 more weeks.  pH readings are coming up similar for all samples using the pH test strips, and in the higher end of the range. Will retest with pH meter and seek additional soil information from farm partner. 

**CRARS Update**
- Two SRA offers extended and accepted, 1 FT temp and 1 PT.  Two student employees hired to work on maintaining the CIG plot, will alleviate some of the workload on Seth and Marco.
- Priya and Cindy collected 48 butternut squash and 96 paired soils from 20-year-old till/no till field at Five-Points near Fresno CA.  
- Extra 6 squashes were collected so that storage time could be assessed.  All 6 squashes were from the same treatment and will be processed for intake and wet chem -- 1 squash every 2 weeks to detect changes in nutrient values with increasing storage time, beginning once MD is complete.  
- Butternut curing interval 10-14 days (at 80-85 F); day 14 is November 4.  Storage (at 50-55 F) in an AC-cooled and insulated shed/container at the farm.

**Hardware/Software, Process Issues**
- Cindy’s question about the interpretation of the pepper data.  Greg working on a tool (data exploration dashboard) https://farmersfeedback.herokuapp.com/pages/inner?explained=antioxidants  Certain crops show specific trends under specific management practices (grapes for instance).  2019 data shows different relationships – varies from crop to crop
- Peppers were surveyed under the Chico Lab Testing organization (confusion overlapping with 2019 device and 2020 survey use…)  Surveyed with 2020 surveys; sample collection was filled out for 2019.
- Approach to FRAP and polyphenols surveys with respect to timing and the reaction for each process -- Katie would run 40, Mason would run 80.  Chico hasn’t had more than 1 batch of 25 (up to 36 with butternut MD), but will have multiple batches of wet chem with butternut and brassicas to come.
- Lettuce & mizuna 2020 on 2019 surveys – still have extracts that we might want to rerun, how to approach?  Mizuna – rerun on 2019 because it is an “other”, run on 2019 RFC depreciated

**Next Meeting and To-Do List…**
- Aundrea and Seth to meet with Katie to discuss wet chem data generated using matrix approach for ratio determination and MD next steps
- Dan and Aundrea: potential meeting to arise from pepper data
- Katie: SOP update to reflect caution with crystallized F-C reagent
- Dan and Greg to merge scripts to build a dashboard for interim results
- Dan to build research/university survey for sample collection that better captures replicated treatment data to help minimize repetitive data entry.  Dan and Seth to continue coordinating



### October 19, 2020 Meeting Notes
_Katie, Priya, Aundrea, Dan, Seth, Benoit-Pierre_

**Lab Updates**
- Ann Arbor – New employees doing well. Katie had to quarantine for a week, but no one got sick.  The box of supplies sent to France went missing – count as a loss and move on.  New devices are ready.  Fridge is empty, but expecting more samples today.  Lots of work to do other than intake; soils, XRF, etc.
- Chico – Till pepper wet chem completed last week.  Compiling pepper data for ARI meeting Fri 10/23.  Working on soil analyses at present.  Filed paperwork to use lab with BOD incubator to proceed with respiration.  No response from Dean or instructional staff yet.  
- France – Continuing to analyze carrot samples, soil analysis for LOI and pH.  Working on wet chem next week.  Aundrea mentioned F-C reagent crystallization as possible source of polyphenol error, Katie to update SOP.

**CRARS Update**
- Awaiting response from HR for hiring SRAs and office manager – going on 3 weeks.  Center will have 2 FT SRAs.  Marco got into Master’s program and is leaving in Dec or Jan.
- Correction: Researcher with 20-year-old till/no till field currently growing butternut squash, not yellow squash.  Five-Points near Fresno CA.  Collect butternut samples: 48 samples w/ 96 soil samples.  Priya to share specs for storage with Katie.  Butternut needs method development (MD).

**RFC Update**
- Dan: Sample counter to total up number of samples through intake.  Includes splits.  Goal was 3500 for the year, currently at 2000.
- Grantham: Negotiation ongoing with lawyers, but moving forward

**Hardware/Software, Process Issues**
- Peppers were surveyed under the Chico Lab Testing organization (confusion overlapping with 2019 device and 2020 survey use…)  Provide Dan with a list of Lab IDs/Sample IDs.
- Grant and interim reporting – how best do we cull the data from the csv files if the All Data or individual dashboard isn’t ready for 2020 and beyond?  Greg and Dan had a discussion about this.  Need to build dashboard that merges different scripts to present results. 
- Time/effort required to populate FarmOS and/or SurveyStack – can we reduce or minimize data entry?  Not just for Chico lab but for external farmers/research partners.
  - Weekly management forms – can skip forms when nothing happens during that week.  In-season management – lots of repetition with data.  For the weekly form, select the plantings that a given management practice (e.g. irrigation) applies to and submit it once.  
  - FarmOS planting forms are doable for Chico farm plots.  But sample collection forms will be a heavier burden (320 samples within a few weeks).  For each planting form (16 total for brassicas and lettuce), Chico will collect 20 samples, therefore how to simplify data entry?  Dan: May not have a solution this year, didn’t anticipate replicated research.  Possibly use potato research survey and apply it for Chico’s use?  Could create “Research/university partner” survey? Chico anticipates research to be of larger production scales, likely highly replicated, with different treatment types.  Need ability to take at least 20 samples; what additional info is needed?  For example GPS locations?  Could have a question that allows branching in survey (collect 1 GPS point vs. multiple GPS points?). Timeframe for next use would be mid Nov (brassicas).  Dan and Seth to work on this offline.
  - Seth to discuss with Priya whether Chico will continue to partner with Five-Points so that FarmOS data entry (planting forms) can be completed for butternut and future crops with (hopefully) a single data entry effort
- Separate discussion/meeting on butternut squash MD?  Katie advises that we test wet chem trials all at once.  Likely to be high in AOX.  Collect as many “varieties” as possible.  Grate? Grate and then grind…?  Could conduct wet chem on different intake procedures also.  Aundrea to share approach document with Seth, Priya, and Katie.  Meeting likely to follow.

**Other Items**
- Follow ups: Full spec, cuvettes, and new reflectometers sent to France?--Lost

**Next Meeting and To-Do List…**
- Cross off To Do List: Aundrea to check in with Libby Gustin for farmer samples.  Cindy and Priya had meeting Libby last week, setting up an IC agreement so that Libby can help with literature search.
- Aundrea to provide list of pepper IDs to Dan so that data can be moved from Chico Lab testing organization to RFC production side database.
- Aundrea to share butternut MD approach document to address concerns before proceeding; meeting to discuss likely to follow
- Aundrea to follow up incubator request after add’l Plumas rooms request has been approved by COA, CSE, EHS, and EOC.
- Katie to SOP update to reflect caution with crystallized F-C reagent
- Priya to share produce cooler specs with Aundrea and Katie
- Dan and Greg to merge scripts to build a dashboard for interim results
- Dan to build research/university survey for sample collection that better captures replicated treatment data and help minimize repetitive data entry.  Dan and Seth to continue coordinating

### October 5, 2020 Meeting notes
_Dan, Greg, Aundrea, Katie, Benoit-Pierre, Priya_

**Lab Updates**
- Ann Arbor – Hired 2 new people. Hiccups with grains (1 batch)—worked out the bugs and expects to be in good shape for proceeding.  Dilutions: blueberries, kale and peppers need to be diluted frequently.  Spinach needs to be diluted 70% of the time for polyphenols.  Calculations based on existing dilution rate.  Going forward, AOX analysis for blueberries, kale, and peppers will be diluted first and run at standard (1x) dilution if too low.  Polyphenols analysis of kale, spinach, and blueberries will be diluted first and run at 1x dilution if too low. 
- Chico – Seth leading transplanting of brassicas (kale, mustard, and mizuna) on Oct 5-7 for comparison of till/no till management and both treatment and no treatment with BEAM compost extract application.   Little gem lettuce to be planted Oct 12.  Aiming to harvest brassicas and lettuce late Nov/early Dec.  Extracted (till) peppers on Thurs, Sept 30; wet chem to proceed Thurs, Oct 8.  Comparison of till v no till managements but till treatment peppers were planted 30 days later than no till.  
- France – Continuing with analysis of samples. Anticipating carrot and leek samples. Aims to pull data from the database once analyses are complete.  Full spec, syringes, cuvettes ready to ship; just waiting on reflectometer (bionutrient meter).  Our Sci now has enough parts to make 50 devices; telfon standards are being outsourced for construction.

**CRARS Update**
- SRA interviews are concluded.  Finalizing SRA hires today; permission to move forward with interviews for students.  Researcher with 20-year-old till/no till field currently growing yellow squash – collaboration for ND, TBD.

**RFC Update**
- Grantham funding – still waiting!  Lab on good track to hit yearly target (samples); 2019 data getting wrapped up, report writing still to come.

**Hardware/Software, Process Issues**
- Follow ups: SOP pics, France’s full spec and cuvettes, new reflectometers, QC soil for pH and respiration. Aundrea to follow up with Katie for pictures requests.
- Data sent by Chico testing lab for respiration can be plugged into a spreadsheet and results can be calculated.

**Other Items**
- Libby Gustin reached out to see if she could participate.  Aundrea responded to see if Libby had farmers willing to participate, awaiting Libby’s response. – No new update, still awaiting response.  Aundrea to email for an update.  Priya to schedule an appointment with Libby to discuss collaboration with CRARS.

**Next Meeting and To-Do List…**
- Aundrea to follow up with Katie for SOP pictures requests
- Aundrea to check in with Libby Gustin for farmer samples
- Katie to send full spec, cuvettes, and new reflectometers to France
- Katie to send QC soil for pH and respiration to Chico


### September 21, 2020 Check-in meeting notes
_Katie, Dan, Greg, Aundrea, Seth, Benoit-Pierre_

**Lab Updates**
- Ann Arbor – No new samples this last week, but busy with grains.  Averaging 160 samples per week, bought a new freezer to accommodate backlog.  Review in progress to revise MeOH ratios with produce.  Dan would need to check on any variation to MeOH volumes.  Produce mass, however, is adjusted in survey calculations.  Katie is conducting interviews for an open lab position.
- Chico – Completed pepper wet chem on Friday, Sept 18.  Soil processing and scanning scheduled for this week.  Hold on pH and respiration for QC soil.  Aundrea to coordinate with Katie on shipping samples, timing likely to be after second round of pepper analysis.  Mentioned that dilutions ended up consuming the rest of the 10-100 ul pipette tips, so an order request is forthcoming.  Katie recommends pre-order supplies at least a month in advance given large demand for scientific supplies and increased shipping times.
- France – First samples last week – processed intake of carrots, potatoes, and zucchini.  Extraction tomorrow.  Can also proceed with LOI and pH, but package containing reflectometers still hasn’t arrived so no scanning or wet chem yet.  Katie will send new reflectometers and cuvettes so that Benoit-Pierre can proceed with AOX and polyphenols.

**CRARS Update**
- Open positions for SRAs and students; interviews scheduled for this week for temp and permanent SRA positions.  Two student positions open and likely to be farm only staff through the end of the semester.  
- Reviewed timing for pepper harvest, transplanting and harvest of brassicas and lettuce, and analysis through the end of the year.  
- Libby Gustin may find other farmer participants and could send RAD-Lab samples between now and end of the year, TBD.

**RFC Update**
- Grantham committed but interested in long-term collaboration (multiple years and millons of $$) – collaboration in process.  Purpose would be to build another lab that would have much higher end instrumentation (e.g. GC-MS) for R&D that would calibrate instruments in RFC Lab.  R&D Lab would help develop next gen devices; funding for lab processes and support grad students.  Basic processes not likely to change much in the short-term. 

**Hardware/Software, Process Issues**
- Reminder that Chico needs QC soil for pH and respiration.
- Lots of proposed updates to the SOPs as a result of pepper analyses!  Initiate discussion on slack channel, then add specific changes to "SOP change record" for tracking and approval.  Changes can be incorporated on a monthly basis.  
- For SOP doc that was posted to GitLab before June, photos were helpful for reference, now on longer available.  Make specific photo requests on SOP slack channel.  Photos to be added to web-based version; intake looks good, but other sections need pics.
- Reflectometer updates – 50-60 cases prepared.  Boards to be tested by manufacturer. Failure rate should be way lower.  940 nm LED intensity is high, Greg to decrease intensity to get a better result.  Teflon calibration pieces to be constructed.  
- Full spec for France lab still needs final checks before shipping.
- Wet chem reflectometer – RAD-Lab used range calibration 2 which saved over original calibration.  Continue using established process to be consistent with upcoming pepper analysis.  Send pepper data to Greg to QA/QC.

**Next Meeting and To-Do List…**
- Katie will send new reflectometers and cuvettes to Benoit-Pierre
- Aundrea to send order request to Katie
- Aundrea to send Greg pepper data
- Greg to construct teflon calibration standards
- Chico awaiting QC soil and new reflectometers
- France awaiting full spec
- Web-based SOPs need updating with photos




### September 10, 2020 Make-up Check-in meeting with Chico
_Seth, Greg, Katie, Dan, Aundrea_

**Lab Updates**
- Ann Arbor – Mason completed grains SOP, Katie reviewing.  Blueberries now in SOP.
- Chico – In-person instruction moves to all virtual due to COVID cases on campus.  
- France – needs stopcocks and full spec.  Katie submitted an inquiry into package that shipped and went missing.

**CRARS Update**
- Fall Brassica restart – kale, mizuna, mustard and spinach seeded last week. Spinach w/ low germination, will be replaced with lettuce, timing of anticipated planting staggered by about 2 weeks.  Lettuce seeding to occur Monday.  Restart postpones methods development for head-bearing Brassicas.

**Hardware/Software, Process Issues**
- Experimental design (#s of treatments and replicates) as applies to Sample Collection Survey in SurveyStack: “Where is the sample coming from?” informs the collection data for # of samples from a collection event.
FarmOS – Use planting form instead of using the survey stack sample collection survey.  Dan and Seth to meet and discuss inputting OVP management data in FarmOS.
 - Regarding methods development, ideally select produce and have methods worked out prior to growing crops so that there is sufficient time to test intake & wet chem process, and have completed methods updated to OurSci/RFC Lab-side (SOPs, surveys, data management, and other interface & back-end processes).

**Other Items**
- Slack Channels for communications/sharing/social media
- Updating SOPs – updates made to wiki, ideally with changes highlighted (track), Greg to explore options.  SOP channel in slack for small changes or major questions for discussion.
- Libby Gustin reached out to see if she could participate.  CRARS discussed her participation internally, but Aundrea uncertain whether that moved forward – check with Cindy.  Chico to circle back and let Ann Arbor know if we will receive samples or if they should be shipped to Ann Arbor.

**Next Meeting and To-Do List…**
- Next check-in Sept 21
- FarmOS meeting to be scheduled

### August 24, 2020 Meeting Notes
_Katie, Priya, Aundrea, Seth, Benoit-Pierre_

**Lab Updates**
- Ann Arbor – Finalized the grain MD (oats and wheat). Working on soil scripts for 2020 – calcs may not be working, Katie needs to talk to Greg about it.  Produce running well.  
  - Updated SOPs are now on the website.  Katie shared the GitLab link: https://gitlab.com/our-sci/rfc-docs/-/wikis/Lab-Process
  - Including blueberries now.  Blueberries create a gel when ground, use non-chlorinated paper towel as a filter in the garlic press to get juice from pressed berries.
- Chico – Wet chem refresher/training last week, notable differences between 2019 and 2020 device, calibration, and surveys. Another meeting scheduled to review wet chem (Thurs 8/27, 8:00)
- France – Has not yet received supply packages from Katie.  Intake can proceed but skip juicing and freeze and hold wet chem samples.

**RFC Updates**
 - none this meeting

**CRARS Update**
- Signed MOU, method development for Brassicas

**Hardware/Software, Process Issues** - to be addressed at meeting on Aug 27
- Chico to do more regular additions of the “Lab issues to address” file
- Device app: unable to login with gmail address
- FRAP procedure ran ok, but extract analysis indicated a dilution was required.  Initial run in Jan did not -- could be affected by high room temp?
- Polyphenols procedure hung up on standard curve – 2 attempts with R2 < 0.99 

- Reflectometer charges slowly, appears to discharge faster than can be charged.
- Also runs hot. Room temp is high ~26 C, but instrument records 29-30 C and gets hotter by ~0.1 C each scan
  - New reflectometer has temp sensor that doesn't work properly
  - Return old reflectometers for update and battery change

**Other Items**
 - Libby Gustin reached out to contribute to the project. Discuss at CRARS research team meeting, then coordinate accordingly with Dan and Greg. Katie suggests waiting to ship leafy greens when weather cools (ship time too long with postal delays for greens to ship without refrigeration at present).  

**Next Meeting and To-Do List…**
  - Process review meeting 8/27/2020 at 8am PDT/11am EDT
  - RFC check-in meeting currently scheduled for Labor Day, reschedule...
  - Aundrea to ship reflectometers (and other equip/samples) to Ann Arbor


### August 10, 2020 Meeting Notes
_Priya, Dan, and Aundrea_

**RFC Updates**
- Grantham Foundation proposal: increasing scope and funding.  RFC in discussion with MIT for development of tech, location(s) of lab(s). New analytes and tools: Vitamin C, fiber, new hardware, calibration of tools. 

**Lab Updates**
- Ann Arbor – Finishing script development for soil samples (batch calculations) – testing scripts for calculations, about a week from being finalized.  Final produce sample calculations in development, to be stand alone survey – could be a one-by-one process (aggregates all produce data from pervious surveys and show users any errors and outputs final Anitoxidant, Poly, etc values).  Grain method development almost complete.
- Chico – Approved Return to Campus process; staffing up to 3 people in Plumas 325 and up to 6 people at the SPA.  Ramp up soil analyses; RFC samples TBD.  Chidimma no longer working with us; open position advertised for Staff Research Associates.
- France – Considering process for receiving samples from farmers.  Supplies from RFC Main Lab hung up in transit.

**CRARS Update**
- Reschedule meeting w/ Greg and Cindy to discuss MOU

**Hardware/Software, Process Issues**
- None to discuss at this time

**Other Items**

**Next Meeting and To-Do List…**
- Dan on vacation next meeting (Aug 24)



### July 13, 2020 meeting notes
_Dan, Katie, Aundrea_

**RFC updates**
- Grantham still moving forward, not yet approved.  List of things to investigate (e.g. vitamin C), protocols to be developed.  
- Media outreach: video in RFC lab – Katie needs notice.  Grains funded through B of A, Pipeline Foods; part of funds to do a video visit ~last week of July, to be included in a series for media releases (e.g. podcasts, articles, etc.).

**Lab Updates**
- Ann Arbor – slowly getting samples in, training new personnel. Working on grains – some need to be dehulled, use mortar and pestle to remove hulls, moisture content value needed. Grain protocols in development.  Katie has a box of supplies for Chico (syringes, cuvettes).
- Chico – Return to Campus Process, iterations of the work plan document, switched to formalized process with more approvals necessary, going through multiple iterations of that, to be continued...
- France – On holiday, not attending.  Dan ordered SiWare spec, found CO2 sensor at MSU, which can be added to next shipment of equipment -- need access to MSU campus to retrieve.

**CRARS update**
- Rescheduled: Meeting w/ Greg and Cindy to discuss MOU -- Aundrea to bring up at CRARS research team meeting 

**Hardware/Software, Process Issues**
  - protein wet chem for grains, + potatoes (Groups interested in potato protein - American Potato Research Institute, Foodminds, Potatoes USA). Gather data on potato protein quality, recognizing total protein is low.  Groups are focused on human health outcomes, more discussions/work likely 2021.  Lowry method being trialed, likely time consuming.  
 - RFC previously trialed Bradford method for carrots and spinach; struggled to get strong calibration, had low r2 values, requires produce pellet to be dissolved in NaOH, which requires larger quantity of NaOH than MeOH (>15 ml).


**Other Items**
- Clarify organizations on the web app/device app
  - Chico Lab Testing organization is for method development work
  - Depreciated RFC org: archive older surveys that are no longer being used, but still allowing access to them
  - For Non-RFC soil respiration tests, which org is appropriate? Use Chico Lab Testing org, "2020 soil respiration survey" - Dan to copy existing 2020 survey.
  - Dan to make intake surveys (leafy greens & other produce) for practicing intake – throwaway surveys

**Next Meeting and To-Do List…**
- Katie has a box of supplies for Chico
- Aundrea to remind CRARS of MOU meeting/discussion
- Dan to populate Chico testing org with surveys for soil respiration and produce intake practice

### June 29, 2020 Discussion
_Greg, Dan, Katie, Benoit-Pierre, Aundrea, Priya_

**RFC updates**
- Grantham funding; internal discussion on spectroscopy.  Interested in publishing opportunities where studies use data generated by the project.

**Lab Updates**
- Ann Arbor – 
  - ~100 samples processed already
  - Experiencing very slow shipping (should be 2 days, is 6 days) ... everyone 
  - new devices, one has already been shipped to France (via USPS). First batch of devices doesn’t have thermometer, calibration is different (not just drift check).
  - Ran into wet chem issues with new devices – could be just an updating problem; TBD.  Not all surveys were updated for the new devices.  Use old devices with 2019 surveys, and new devices with 2020 surveys. 
- Chico – Return to Campus Process
  - CSU campuses released draft reopening process in early June. Finalizing forms for re-entry this week, still requires approval process.
  - Not running samples yet, maybe by August?
  - Muffle furnace/LOI, SPA installation is off the table. Two options: 1) Plumas furnace, need dimensions.  2) If sending to Ann Arbor, give notice of month so they can be factored into workflow.
- France – 
  - First week of August to run first samples.  
  - Needs to do SOP and run through wet chem.  
  - SPM in lab, 400 – 2000 nm; can use it to check wet chem, just run calibration curves.

**CRARS update**
- Progress report on RAD-Lab business plan - meeting with CSE CEO; Garrett Liles presented powerpoint of business plan, went well by all accounts.  
- Meeting w/ Greg and Cindy to discuss MOU - cancelled due to short-notice inspection.  Need to reschedule.

**Other items**
- Adding watermelon to produce types: 
  - Greg recommends very diverse initial sample set of 50-100 samples (ideally lots of varieties and growing conditions).  
  - Need clearly defined study question.  
  - Surface scan would be infeasible; would be interior only.  Juice would work fine.
  - How valuable is WM with respect to AOX and polyphenol?
  - Shipping and storage problems, but could be something for Chico to take on.  
  - Need to write up a plan of action/experimental plan and have partner fill in the details.
- Proper IDs: Lab ID 11 = Ann Arbor, 12 = Chico, 13 = France; also batch IDs, and sample IDs/barcodes to be printed and distributed.

**Hardware/software, process issues**
- Mason - do a dilution + run entire wet chem process to make sure it works + outputs expected values, and share on slack so we can see exactly what it looks like.  
- Please do not run new surveys on an old device - get a new device for the new surveys.
- If you only have an old device, either wait or skip those items until you get a new one.
- New reflectometer has removable mask for round fruit, different calibration method; more development to be done. Teflon piece scanned for calibration.  Circuit boards tested successfully.
- CO2 sensor delayed, TBD.  Could be fall instead, and would still need testing and comparisons.


**Next meeting and To-Do list…**

- (priya, greg) reschedule meeting 
- (greg) get CO2 sensor to France (test), produce another 2 devices for labs
- (greg) buy + send siware sensor to Benoit-Pierre
- (greg) send out Aundrea examples of previous proposals to support watermelon potential...
- (katie) update SOPs with IDs + related, print + ship all IDs needed for this, on a spreadsheet track what was shipped to who when (0 - 1000).

- Discussion -

### June 15, 2020
_Katie, Aundrea, Benoit-Pierre, Dan, Priya_

**To Do's**

- Katie to share checklist for intake processing, social distancing in the lab protocol?

**RFC Updates**

- Octavio summarizing data analysis, getting community programs up and running

**Lab updates**

- Ann Arbor – several ppl permitted in the lab at once, 6-ft social distancing requirement; if exposed (within 6 ft for >10 mins), then 14-d quarantine
- Chico - Power upgrades complete, strict 1-person per room requirement on campus, onion harvesting to come next week
- France - testing processing methods and using the reflectometer with wide cuvette; sonicator needs cooling. Katie to send supplies. Benoit-Pierre to be on vacation July 13-31.

**CRARS update**

- Progress report on RAD-Lab business plan - hopefully wrapping up this week. Nutrient density as part of services we provide.
- Last week's meeting w/ Cindy Daley to discuss financials w/re: MOU and commitments 

**Hardware/software, process issues**

- Shipping challenges to Ann Arbor (degrading sample material), MeOH ratio table revised, surveys under Katie's review – but likely to need further modification when Chico and Valorex start to use
- Greg redesigning the reflectometer – boards are ordered, 3-4 wks to ship from China after ordering; CO2 sensor – redesign in July to 1) improve and better seal chamber to reduce volume and time needed – currently consistently 40% off, and to 2) improve methods with moisture volumes and other parameters


### June 1, 2020
_Greg, Dan, Katie, Aundrea, Benoit-Pierre_

**To Do's**

- (greg) email Priya / Cyndi / Aundrea connect re. MOU

**RFC Updates**

- Octavio beginning on project management dashboard for sample tracking
- Reflectometer redesign - 
- Soil respiration - 
- Updates on prediction
- Funding through Grantham Foundation - may help fund publication work.

**Lab updates**

* Ann Arbor
  - first day accepting samples!
  - Mason / Katie are finalizing last updates to protocols w/ antiox and polyphenol dilutions
* France
  - got in centrifuge, laptops are next
  - cross check may not be possible to run with soil due to COVID restrictions
    - but Valorex can send us soil samples to compare
    - and Valorex needs to send Ann Arbor XRF samples anyway...
  - can we walk through SOPs with Katie
    - Katie will do video of process, then do video chat, then discuss...
  - send package out now
* Chico
  - lab closed again this week, harvest + upgrades!
  - going through process for getting computers in their lab (work orders)
  - have to socially distance - so it'll be limited people in the space

Agenda/minutes
- agenda to be distributed as doc via email for the time being; minutes to be posted to GitLab

* Chico working on business plan for their lab - not done but moving along!
* Talked through graduated cylinders for measuring volume

### April 27, 2020
_Greg, Dan, Katie, Aundrea, Priya listening in_

**To Do**
- Greg - 1) GitLab invite for Priya to review meeting notes and other docs, 2) provide public method links in RFC knowledge base (Lab Testing and Survey reference documents)
- Katie and Aundrea - separate meeting to discuss new produce types
- Dan - update Knowledge Base Documents (Lab Testing and Survey)

Lab updates
- Chico – work plan submitted to COA Deans and Farm Management, additional higher level approvals required
- Michigan allowing low risk activities to resume, strict 1 person at a time in lab to tentatively start next Monday (TBD)
- France has most of their equipment ordered/received, but no info on lifting lockdown
- separate France group doing data analysis: reflectometer data, NIR data; csv (1000+ column spreadsheet available)

Agenda/minutes
- agenda to be distributed as doc via email for the time being; minutes to be posted to GitLab

MOU and Yearly Plan update
- CRARS internal budgeting discussion ongoing
- business plan to be drafted for RAD-Lab to make it self-supporting
- return to address MOU and Yearly Plan when Lab becomes operational again

Lab survey, Lab testing (Knowledge Base documents) needs updating
- reference documents, may be similar to documents from 2019
- info on motivation and rationale for structure of RFC, soil and food testing

Follow-ups for 6-mo cross check program
- Aiming to do cross check in August and December
- France – customs regulations for shipping of samples, permit(s)? – info back, need to find company to do & can be done without fees
- Katie, needs more time to find other 5 locations for soil samples, Dan and Greg to follow up with RFC members; needs at least 2 kg
- Chico to provide QC soil as one of the 7 locations (1 other location already determined)

Progress on data visualization (Octavio) and data analysis (Dan)
- https://app.our-sci.net/#/dashboard/by-dashboard-id/rfc-2020-onboarding
- above link for onboarding new participants (goals for # of samples of given produce type, regional breakdown)
- additional models likely available by end of week
- Data analysis of AOX and polyphenols concentrations (concs) - completed and confident that concs are good
- Tocopherols concs are currently overestimated, needs further method development before adding to standard protocols

Updated Project Plan
- https://docs.google.com/spreadsheets/d/1c_WoJIf1C24WJdS2xVyTIz49HDnEmzCAW4K41HIcQO8/edit?usp=sharing
- 15 produce types including blueberry, apple, bok choy, mizuna, swiss chard, beets, potatoes, leeks, zucchini, yellow squash, spinach, lettuce, kale, grapes, and carrots
- Feedback on produce types (processing, homogenizing), which ones should we focus on?
- Aundrea and Katie to have a meeting to discuss details of produce processing
- Tomatoes - Chico, others w/ strong interest in continuing tomato analysis and expanding beyond cherry tomatoes
- RFC/Our-Sci would need help in organizing and shouldering burdens of tomato logistics (aggregating data and shipping), plus further method development to expand tomatoes
- Analysis had been limited to cherry tomatoes in order to understand variation at smaller scale, but would want separate project to expand and connect tomato analysis to larger experimental design (e.g. hydroponic vs regenerative)
- Touching on mizuna - scans may be problematic given finely divided leaf structure
- Touching on leeks - high value crop, 3 color zones - is color predictive of nutritional content? Other Alliums (garlic, scallions, shallots, and bulb onions) are off the table
- Intake survey to include a density calculation for non-leaf produce types - does size matter to nutritional content (e.g. zucchini, potato)?
- Leafy produce will still be measured using leaf length



### April 13, 2020 (agenda from March 30, 2020)
_Greg, Dan, Katie, Aundrea, Priya, Benoit-Pierre_

**To do**
- Greg and Dan - Katie needs 5 other locations for soil samples
- Chico to have internal discussion regarding MOU and Yearly Plan
- France to determine customs regulations for shipping of samples or permit requirements
- All - provide feedback on produce types

Lab updates under lockdown
 - Chico - Submit a work plan, subject to approval, to regain campus building access and continue research
 - Ann Arbor - stay-at-home order until April 30, new vegetables while at home, documents
 - Katie is awaiting lifting of the stay at home order to go back to the lab
 - France - 1 month stay at home
 - Produce sample receipt pushed back until at least June 1

MOU Review
 - Chico - Online conference hosting had taken up most of CRARS bandwidth, therefore internal discussions have not moved forward
 - Greg would like to meet and discuss in ~3-4 weeks to walkthrough and sign

Six-month Cross Check Program
- See To Do items
- Chico reviewed and commented on SOP

Web-app discussion - dashboard walkthrough
- Raw data is not recommended for review – use the dashboard
- Greg to link dashboard on website later
- High and low data types would be flagged
- Test data is plotted (histograms) for total samples
- AOX and Polyphenols are complete
- XRF in shop and incomplete
- "All data human-readable" (recommended for review)
- "All data complete" includes non-human-readable data for analysis
- Next steps: Run prediction models

Ann Arbor Updated Project Plan:
- Dropping cherry tomatoes, keeping grapes
- Adding swiss chard, bok choy, mizuna
- Looking at beets, potatoes, leeks, summer squash (zucchini and/or yellow squash)
- Keep or discard mid-rib of greens?
- Mid-rib adds noise
- Need consistency of processing between produce types

Next meeting
 - Feedback on produce types (processing/homogenizing…?), which ones should we focus on?
 - Prediction modeling for wet chem, minerals data to come later/data analysis 
 - Lab survey, Lab testing (Knowledge Base documents), walk-through and feedback (skipped on meeting agenda)

### Feb 4, 2020
_Greg, Dan, Katie, Aundrea, Priya_

**To do**
Greg - design tests to add 'blanks' for anti and poly


Chico
- CO2 sensor
  - comparison between Our Sci CO2 sensor and their CO2 sensor (24 hour method) are very comparable.
  - had a discussion about CO2 sensors and next steps for April.
- Muffle furnace
  - its ordered and in stock... but having issues finding a place to put it
- Chard testing
  - started, but errors in anti and poly means they kind of need to restart.  
  - good reminder for Ann Arbor to text added 'blanks' for anti and poly to account for background color of the sample 
- Data management
  - add a 'is this a test' question, then allow an experiment ID if it's a test question

Ann Arbor
- updates
  - Considering getting CN analyzer
  - considering adding 'task based' surveys
- Other Learnings
  - Acid bath not required for 15ml tubes, but ARE still required for culture tubes
  -