# Soil Grinding and Sieving

**Table of Contents**

1. [**Decide on a Sieving Method**](#decide-on-a-sieving-method)
2. [**Soil Drying**](#soil-drying)
3. [**Manual Sieving**](#manual-sieving)
4. [**Sieving with a soil grinder**](#sieving-with-a-soil-grinder)

## Decide on a Sieving Method

Soil sieving can be conducted through two methods:

   * Manual sieving
   * Sieving with a soil grinder

Either method is acceptable and dependent on the needs of the individual lab.


## Soil Drying 


### Required Equipment
- Snack bags
- Aluminum Baking Tray (9x13)

1. Samples are generally received in snack sized plastic baggies.  Upon receipt and after moving through the Sample Receiving survey, open up the sandwich bag containing the soil, folding the top part towards the outside of the bag, so that there is as much surface area as possible open to the air.  Place with other samples in an aluminum baking tray and air dry.

2. Place each tray on a drying rack. 
 
3. Samples with high clay content will dry very hard and will be difficult to sieve, break these up as much as possible prior to drying. If possible, try to break up the sample on a daily basis for these samples to make it easier to break up when dry. 

## Manual Sieving

### Required Equipment

- Soil sieve, 2 mm mesh (#10), 8-12 inch diameter
- Morter and Pestle
- Paint Brush and/or Cleaning Brush (9x13)
- Aluminum Baking Tray

1. Once samples are fully air dry, they can be sieved.  
2. Place the 2mm sieve in the aluminum baking tray. 
3. Pour the dry soil out of the bag into the sieve and shake.  
4. If the soil all falls through the sieve, pour back into the bag and you are done. If there are larger particles and rocks, etc. This can be poured into the mortar and pestle and ground down into smaller particles.
5. Repeat the process of sieving and grinding the samples until all of the soil has gone through the sieve.  
6. Discard any rock or vegetation.
7. Clean the sieve with the paint brush and/or cleaning brush. 
8. Be very careful with the sieve. You can use the pestle to help push the soil through the sieve, however, do not apply too much pressure to the sieve or you may ruin the equipment.

## Sieving with a soil grinder

### Required Equipment

- Soil Grinder
- Shop Vac
- Shop Vac Crevice Tool
- 0.5 oz (1 tbsp) plastic portion cups with lids  

1. Ensure appropriate PPE is worn during this process, including goggles and dust mask.  Gloves and an apron or lab coat are optional.

2. Attach the crevice tool to the shop vac hose.  Place the crevice tool into the soil grinder between the U-bracket and the hopper so that it will be in a position to draw in the dust produced by the grinder. The grinder produces a tremendous amount of dust and this method reduces the dust significantly. 

3. Remove the lid. 

4. With the gate in place, add the soil to the hopper, and replace the lid.

5. Place a 0.5 oz portion cup into the soil bag and place the soil bag over the chute, attempting to keep the portion cup in the center of the bag. The goal is to collect a representative sample of homogenized soil.

6. Switch on the shop vac and the soil grinder. 

7. Slowly remove the gate to allow the soil to be ground. 

8. The grinding process should only take about 10 seconds.  Do not allow the grinding to continue longer than this to avoid inadvertently grinding large pieces of rock. Switch the grinder off as soon as this process is complete. Visually check that there are no large pieces of clay left in the hopper.  

9. Ensuring that the soil grinder is switched off, remove the crevice tool from the U-bracket, place it into the hopper and remove any large pieces of rocks and residual dust that are left in the hopper.

10. Place the lid on the portion cup and seal the soil bag.

