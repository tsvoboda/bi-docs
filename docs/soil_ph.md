# Soil pH

### Required Equipment:

The survey you will need for this SOP is **2020 pH**. This survey stores information for 36 samples.

- 20 mL scintillation bottles w/caps, 36 bottles for 1 batch
- Bottle racks
- pH strips (pH 5.5-8.0) (Hydrion brand)
- Shaker Table
- An 8 mL dispenser and storage bottles
- DI water
- Tweezers
- Balance
- [pH Manifest sheet to keep track of samples](https://gitlab.com/our-sci/resources/-/blob/5261f6b30a2d67d899d9e95d2759c7745927063b/resources/soilpHManifest.pdf)

1. Use a pH manifest form to track the samples, you can help to orient the rack by adding a black dot in the upper left hand corner and use that as your starting point.

2. Check the alternatives below to see if they apply to the current sample. If not, weigh 4 g of pre-sieved soil into a 20 mL scintillation bottle. 

     Alternatives: 

     a. High Organic Matter: If the sample is high in organic matter, weigh out only 2 g and update it on the survey. This will end up being a 2 g:8 mL ratio. This is because samples with high organic matter tend to be bulky and less dense. It will soak up most of the water if the ratio is 4:8 and it will be difficult to test.

     b. Insufficient Sample: If the sample is too small but you have at least 2 g available, weigh 2 g into the bottle but still indicate 4 g on the survey. You will be adding only 4 mL of water so the ratio will be the same, just a different volume. The test manifest will have a place to indicate the ratio so that there is a record.

*note: adding labeled caps (2/8) or (2/4) when filling the bottles in the alternative methods can help you keep track when adding the water in the next step.

3. Add 8 mL of DI Water to the scintillation bottle with the bottle top dispenser. In the case of high organic matter, you will still add 8 mL of DI water. If the sample is small and only 2 g have been added, add only 4 mL of DI water so that it will be the same ratio as 4 g:8 mL.  

4. Cap the bottle.

5. Place 20 mL bottles into racks, matching their locations on the manifest.

6. Shake for 30 minutes on the shaker table at 150/min.

7. After shaking, let settle for 1 hour.

8. Using tweezers, add a strip of pH paper to the water in the vial and compare the color change to the chart on the pH strip guide. Record the pH value on manifest.

9. Record the Tray ID in the survey.

10. Scan in or manually enter the Lab ID.

11. Record the water to soil ratio.

12. Record the pH on the manifest and in the corresponding survey.

13. Repeat for all samples in the batch and ensure you hit "Send" once you have completed testing.
