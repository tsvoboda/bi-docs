# Wet Chemistry

**Table of Contents**

1. [**Wet Chemistry Software**](#wet-chemistry-software)
2. [**Develop Calibration Curves**](#develop-calibration-curves)
    1. [**FRAP**](#frap-for-antioxidants)
    2. [**Polyphenols**](#f-c-method-for-total-polyphenols)
    3. [**Lowry Proteins**](#protein-extraction-and-lowry-sop)
    4. [**Supernatant**](#supernatant-sop)
3. [**Software Examples and Terms**](#software-examples-and-terms)
    1. [**How to Fill out a 2020 RFC Lab Wet Chemistry Survey**](#how-to-fill-out-a-2020-rfc-lab-wet-chemistry-survey)

## **Wet Chemistry Software**

There are two primary surveys that you will use for wet chemistry analysis:

**2020 RFC Lab Calibration** - This survey is used to generate the calibration curves for Antioxidants and Polyphenols. This survey must be run BEFORE analyzing samples.

**2020 RFC Lab Wet Chemistry** - This survey will be used to analyze and calculate raw antioxidant and polyphenol values and scan the supernatant.

## **Develop Calibration Curves**
1. See individual wet chem methods for calibration solution preparation.  Once completed, follow these steps to test the calibration curve for each analyte.

2. Indicate which test you are running a calibration on. There is no calibration necessary for the supernatant measurement.

3. Connect to the appropriate reflectometer, run an empty cuvette for a blank.

4. Add 700 ul of the blank to the empty cuvette and press "Run Measurement".

5. Run empty cuvette for standard 1.

6. Add 700 uL of the first standard to the empty cuvette and press "Run Measurement".

7. Repeat for the rest of the standards up to Standard 7, if it applies. Polyphenols will have a blank plus 7 standards but antioxidants will only have a blank plus 5 standards. In this case, just hit next through standards 6 & 7 and move on to the Run Calibration Model page.

8. Run Calibration Model.  Click on "Run Model".  It will bring up data and a graph 

9. The model will indicate the r-squared value and will highlight the r-squared value based on whether the result is acceptable or if it should be re-run; green is good, red needs a re-run, yellow is a warning the value is low yet can still be considered acceptable. If you get an unacceptable r-squared value, rerun the same samples again. If that still doesn’t solve the problem, prepare a new batch of samples, checking that all instructions, dilutions, etc. have been followed. If necessary, prepare fresh reagents and retest until you receive an appropriate value.

10. Submit Calibration Values. This step is required to store the data on the reflectometer. If this is not done, the calibration values will not be applied during the sample testing process. Also, the data will be stored locally on the reflectometer until a new calibration is performed and then the old data will be written over. For example, if you run antioxidants and then complete a calibration for polyphenols, you cannot run more antioxidants without doing a new calibration. It is important to consider this when planning out your testing routine.

11. Send survey. This will upload the calibration records to the database in case oversight is needed and for tracking purposes.


## **FRAP for Antioxidants**

Required Components:  
- Ferric Chloride (FeCl3), anhydrous crystals  
- TPTZ, powder  
- 10% HCl (10% of 12M conc HCl)  
- 5x Acetate Buffer  
- Iron (II) Standard crystals (Iron(II) sulfate heptahydrate)  
- Sodium Acetate (anhydrous)  
- Glacial Acetic Acid

### Part 1 - Solution Preparation

1. 10% HCl is used for the RFC Lab acid bath and can be sourced from the HCl stock solution bottle.  If needed, prepare 10% HCl by slowly and carefully adding 25 mL concentrated HCl to 225 mL DI water. Concentrated HCl is extremely corrosive and has a strong fume, ensure this work is done under ventilation, using appropriate PPE.

2. Prepare TPTZ solution - Can be stored in the refrigerator for up to 2 weeks.

      a. Make up 50 mL of 40 mM HCL:

       i. In 50 mL volumetric flask, add 1.64 mL 10% HCl to at least 15 mL DI H2O, fill to the mark with DI H2O

      b. Add 156 mg TPTZ and invert 20 times to thoroughly mix

3. Prepare FeCl3 solution - Can be kept indefinitely if stored in the refrigerator and monitored for biological growth. Warning: FeCl3 can cause burns through  all exposure routes.  Make up this solution under a fume hood  using appropriate PPE. 

      a. Add 162 mg of FeCl3 to 50 mL DI H2O in a volumetric flask

4. Make up Fe Standard stock solutions - Do not store standard solutions. Mix fresh each time.

      a. Mix Iron (II) standard crystals @ 10 mg/ml in DI water by adding 100 mg  (0.1 g) to 10 mL of water to make Iron (II) solution and vortex thoroughly

      b. Transfer 100 uL of Iron (II) solution to 3.5 mL DI water and vortex thoroughly

      c. Mix up standards in the centrifuge tubes based on the rates below

| Standard | Iron (III) Solution (uL) | DI Water (uL) | Iron Concentration (uM) |
| ----- | ----- | ----- | ----- |
| Blank | 0 | 2000 | 0 |
| 1 | 25 | 1975 | 12.5 |
| 2 | 50 | 1950 | 25 |
| 3 | 100 | 1900 | 50 |
| 4 | 200 | 1800 | 100 |
| 5 | 400 | 1600 | 200 |

5\. Prepare acetate buffer.

     a. Option #1 (Ann Arbor lab):  Make up 24 mL 1x Acetate Buffer, using 5x buffer from FRAP kit (use immediately):
     
       1. Add 5 mL 5x acetate buffer to 20 mL DI water and vortex  
       2. Remove 1 mL 1x acetate buffer to make up 24 mL


     b. Option #2 (all other labs): Prepare 100 mL acetate buffer made from scratch. Save 24 mL for use in the FRAP Reagent (equivalent to 1X Acetate Buffer in option #1) and store the rest in the refrigerator for future use
       
       1. Add 50 mL of distilled water to a 100 mL volumetric flask  
       2. Add 8 mL glacial acetic acid to the solution  
       3. Add 1.18 g of Sodium Acetate  to the solution  
       4. Fill with DI water to the mark


6\. Make up FRAP Reaction Reagent -Do not store standard solutions. Mix fresh each time.  
      a. Add 3 mL each of TPTZ and FeCl3 solutions to 24 mL of 1x acetate buffer  
      b. This will make a total of 30 mL of FRAP reagent. This is more than enough for 40 samples, 6 standards, and extra to test up to 14 dilutions or corrections if required  
      c. FRAP reagent must be used within 3 hrs of preparation

NOTE: If you are testing a large quantity of samples, you can increase the volume of FRAP reagent to 50mL by increasing the ratio of ingredients to the following: 5mL TPTZ : 5mL FeCl3 : 40mL acetate buffer (8mL 5x buffer: 32mL DI Water).

### Part 2 - Prepare the Standards and Samples

1. Line up glass culture tubes for each sample (40), a blank (1) and standards (5) on a rack.

2. Transfer 500 uL of sample supernatant to each sample tube.

3. Transfer 500 uL of each blank and standard from the centrifuge tubes to the corresponding culture tube in the standards rack.

4. Add 500 uL of Reaction Reagent to each blank, standard, and sample tube. Shake each culture tube lightly after addition to mix or vortex. Ideally, you should wait approximately 30-45 seconds in between the addition of the reaction reagent to each tube to allow for the normal time when running the samples on the reflectometer.

5. Wait for 10 minutes, space each sample out to 30 seconds each to mimic the wait between samples.  
<a name = "returnA"></a>
6. If you are testing standards, open the 2020 RFC Lab Calibration survey and follow the instructions [**here**](#calibration). If you are testing samples, open the 2020 RFC Lab Wet Chemistry survey and fill out the survey based on the following [**instructions**](#wetchem). **Make sure that the [rfclab variable](#rfclab) is set to 1.** 

7. Read @ 587 nm of Our Sci Reflectometer.

     a. Wipe outside of cuvette to remove smudges

     b. Read blank cuvette

     c. Add aliquot and read sample; making sure that there are no bubbles in the cuvette

8. Calculate total antioxidants as FRAP value or uM Fe2+ iron equivalents based on standard curve.

### **Waste Disposal**

Per CellBio labs SDS: Waste can be neutralized and diluted for sink disposal. Pour all sample waste into a beaker. Add a large amount of water and add 1M NaOH a few drops at a time while stirring, until there is a color change to a clear to slightly yellowish color and test with a pH strip. Once a pH of 6-8 is reached, this can be disposed in the sink, flushing with excess water.


## **F-C Method for Total Polyphenols**

1. Mix up reagents (if not already made):

     a. Mix 20% Na2CO3 solution - 50 g Na2CO3 in 250 mL DI H2O using a 250 mL vol flask.  Use 250 mL glass bottle for storage (can be stored indefinitely at room temperature)

     b. Mix 1 mg/ml Gallic Acid Stock Solution in **METHANOL**. Using 100 mL graduated cylinder to mix 0.1 g gallic acid and fill to the mark with methanol. Invert 20 times to mix. Can be stored up to 2 weeks at 4 C. Put date made and expiry date on the bottle!

2. Set up glass culture tubes for each sample (40), standard (7) and blank (1) that you are going to run.

3. In separate, 15 mL centrifuge tubes, mix 7 standard curve stock solutions at the following ratios:



| Standard | Gallic Acid (uL) | DI Water (mL) | Concentration |
| ----- | ----- | ----- | ----- |
| Blank | 0 | 4.0 | 0 |
| 1 | 50 | 3.95 | 12.5 |
| 2 | 100 | 3.90 | 25 |
| 3 | 200 | 3.80 | 50 |
| 4 | 300 | 3.70 | 75 |
| 5 | 500 | 3.50 | 125 |
| 6 | 600 | 3.40 | 150 |
| 7 | 800 | 3.20 | 200 |


4. Transfer **3.5 ml of DI water** to each sample (40), standard (7) and blank (1) glass culture tube.

5. Add **200 uL of sample, blank, or standard solution** to each tube.

6. Add **100 uL of F-C reagent** to each tube.

7. Add **200 uL of 20% Na2CO3** to each tube.

8. Vortex each tube after adding the reagents, ensuring not to spill as it is very full. Start a timer after adding both reagents to the first tube. Ideally, wait 30 seconds between each addition to mimic the time between measuring each sample. At this point, if your blank or low concentration standards are dark/blue, you know you have contamination.

9. Cover tubes in the tray with Parafilm. Place tray on shaker table and secure. Place aluminum tray on top of the shaker table and secure. Shake tubes on shaker table for 90-minutes at 150 rpm.
<a name = "returnP"></a>
10. If you are testing standards, open the 2020 RFC Lab Calibration survey and follow the instructions [**here**](#calibration). If you are testing samples, open the 2020 RFC Lab Wet Chemistry survey and fill out the survey based on the following [**instructions**](#wetchem). **Make sure that the [rfclab variable](#rfclab) is set to 1.** 

11. Read @ 850 nm of Our Sci Reflectometer.

     a. Wipe outside of cuvette to remove smudges

     b. Read blank cuvette

     c. Add aliquot and read sample; making sure that there are no bubbles in the cuvette

### **Waste Disposal**

The F-C polyphenol reagent should not be disposed of down the drain.  Place all samples, standards, with reagent added, and all leftover reagent ingredients into a waste storage container.  When the container is full, organize for the appropriate agency to dispose of the laboratory waste.


## **Protein Extraction and Lowry SOP**

**Prepare 0.1 M NaOH:** For a batch of 40 samples, 2000 mL of 0.1 M NaOH solution is required. Add 8 g of solid NaOH to 2000 mL of water and stir.  *Use care when adding NaOH to water.*  

### Protein Extraction

Complete these steps after the grain samples have completed the MeOH extraction process.

  a. After centrifuging extractant and pouring supernatant into a separate 15 mL plastic tube, immediately transfer the grain pellet to a 50 ml centrifuge tube. Ensure the 50mL tube is labelled appropriately. * see Note
  b. Add 50 ml of 0.1 M NaOH solution to each tube.
  c. Hand shake the tube to ensure the pellet is fully encompassed by the NaOH.
  d. Place on shaker table and run for 2 hours 
  e. Centrifuge at 3000 rpm for 15 minutes

*Note: transferring the pellet from the 15 mL tube to the 50 mL tube can be difficult and takes great care.  In order to obtain an accurate result, you will need to make sure you fully transfer the pellet and any that is left on the sides of the tube. To do this, you can scrape the pellet off of the sides with a stainless steel spatula and transfer the bulk of the pellet to the larger tube. Then you can pour aliquots of the NaOH into the tube, shake it up with the cap on, and pour that into the larger tube. Usually, doing this three times will be sufficient. Ensure you are using exactly 50 mL of NaOH and that you do not spill any or add more than the 50 mL. You should do a final rinse of the spatula into the larger tube before adding the last of the 50 mL of NaOH to the larger tube. Take care not to spill as the tube will be VERY full.  

### Lowry Method

The Lowry method must be run immediately after protein extraction for the best results.  Freezing and then thawing the NaOH extraction may negatively affect the results. The Lowry method will be completed using a purchased kit. 

Required Materials

  - Solution A
  - Solution B
  - BSA Standard reagent
  - F-C Reagent
  - DI Water
  - Glass culture tubes

1. Prepare 6 glass culture tubes in a test tube rack for the blank and standards. In a separate rack, set up 40 glass culture tubes for the samples.

2. Prepare the BSA Standard solution in a separate glass culture tube by adding 100 uL BSA to 900 uL of DI water. Vortex to mix well.

3. Prepare the standards in the 6 glass tubes using the following table:

| Standards | DI Water (uL)| BSA Standard Soln (uL) | Standard Concentration (ug/mL) |
| ----- | ----- | ----- | ----- |
| Blank | 200 | 0 | 0 |
| 1 | 180 | 20 | 50 |
| 2 | 140 | 60 | 150 |
| 3 | 100 | 100 | 250 |
| 4 | 40 | 160 | 400 |
| 5 | 0 | 200 | 500 |

4. For the sample vials, add 100 uL of sample and 100 uL of DI water.

5. Prepare the mixture of Solution A and Solution B. To run 46 assays, mix 920 uL of Solution A and 46 mL of Solution B. If different amounts of this mixture are required, the ratio of Solution A to Solution B is 20 uL : 1 mL. **This solution is only active for a few hours once mixed, so use it as quickly as possible after mixing.**

6. Add 1 mL of Solution A + B to each vial and vortex. Allow the vials to sit at room temperature for 10 minutes.

7. Add 100 uL of F-C Reagent to each vial and vortex. Separate the additions by at least 30 seconds to provide enough time between measurements in step 9. Allow the vials to sit at room temperature for 30 minutes.

8. Prepare the reflectometer and cuvettes. Use the **2020 RFC Lab Calibration** survey when testing standards and the **2020 RFC Wet Chemistry** survey when testing samples. 

9. Once the vials are ready, run a blank of the cuvette, then transfer 700 uL of standard into a cuvette and read at 632 nm.

### **Waste Disposal**

Lowry assay waste should not be disposed of down the drain. Place all samples, standards, with reagent added, and all leftover reagent ingredients into a waste storage container labeled **Lowry Waste**.  When the container is full, organize for the appropriate agency to dispose of the laboratory waste.

Leftover NaOH can be neutralized and diluted for sink disposal. Pour all sample waste into a 2000 mL beaker. Add 20% HCl a few drops at a time while stirring with a glass stirring rod, until there is a color change from brown to a milky white and test with a pH strip. Once a pH of 6-8 is reached, this can be disposed in the sink, flushing with excess water. Grain pellets may be disposed of in the garbage.

## **Supernatant SOP**
 
1. This process can be completed at any point after initial MeOH extraction.

2. Record data on the Real Food Campaign Survey Software.

3. Connect Reflectometer via bluetooth to the device.

4. Take a measurement of the blank, narrow cuvette, ensuring it is clean and wiped of smudges.

5. Open the 2020 RFC Lab Wet Chemistry survey and fill out the survey based on the following [**instructions**](#wetchem). **Make sure that the [rfclab variable](#rfclab) is set to 1**

6. Fill cuvette with approximately 670 mL of supernatant directly from the 15 mL centrifuge tube.

7. Take measurement.

8. No calibration curve is necessary for this test.

### **Waste Disposal**

Supernatant waste should not be disposed of down the drain. Place all samples into a waste storage container labeled **Methanol Waste**. When the container is full, organize for proper disposal. 

## **Software Examples and Terms**

### How to Fill out a 2020 RFC Lab Wet Chemistry Survey

1. Open the 2020 RFC Lab Wet Chemistry survey within the Forms section of the Our Scikit app

2. <a name = "rfclab"></a> Go to the Variables tab within the Menu and confirm that the rfclab variable is set to 1. You can create the variable with the "Create" button in the upper right corner.

![image](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/2020%20wet%20chem%20survey.png)

3\. Enter the batch ID included on the rack of sample tubes.

4\. Select the analyte being tested.

5\. Select if this is a standard dilution (1x) or additional dilution (20x, 10x, etc.). Additional dilutions will be done once the standard dilutions are done in a separate survey and the analyte values are too high.

6\. Indicate which row number you are testing. This survey will test 10 samples at a time.

![Picture1](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/wet%20chem%20survey%20qs.png)

7\. Is the QC sample present in this row? Ideally, one QC sample will be contained in a batch of 40 samples.

8\. Enter the ID #, for either Lab Sample or QC Sample. The sample Lab sample IDs will be prefaced by the position in the row, i.e. 1: Lab ID.

9\. If there is an additional dilution, a page will appear to specify the appropriate dilution. If this is a standard dilution, this page won’t be displayed as a 1x dilution will be assumed.

10\. Take a blank measurement of the empty, narrow cuvette, ensuring it is clean and wiped of smudges.

11\. Transfer 670 mL of the supernatant from the extraction tube to the cuvette using a 1 mL pipette and run measurement.

![Picture3](https://gitlab.com/our-sci/resources/-/raw/master/images/BI%20Labs/wet%20chem%20survey%20qs%20cont.png)

12\. Complete these steps for all 10 samples. Once this is completed ensure that you hit ‘Send’.

