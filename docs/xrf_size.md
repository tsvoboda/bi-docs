# XRF Grind Size Comparison

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script>

*For an update please see the [2021 XRF Plan](https://gitlab.com/our-sci/bionutrient-institute/bi-docs/-/raw/master/docs/img/XRF%202021%20Plan.pptx)

## Purpose

The purpose of this test is to determine the precision of the XRF, given grind size, placement variation, and subsampling variation by produce type (carrot, spinach...) and by element (Mg, Mo, etc.) in the RFC.  Once we understand the loss in precision due to these factors, we can apply statistical analysis to learn how much of the variation we see is coming from sample variation at the same grind size, and sample variation between grind sizes.

1. Variation from placement and packing on the XRF
2. Variation from subsampling a single plant (same plant but separated chopped bits into separate dryer cups)
3. Variation from differential grind sizes

## Preparation

(rena) Visually identify the 'coarsest' allowable sample for each produce type in 2019, and the 'finest' allowable sample for each produce type in 2019.  You may want to look through the 2019 XRF bags and physically pull 3 samples for reference:

1. Coarse (least ground you can find)
2. Medium (typical grind size)
3. Fine (most ground you can find)

These will represent the visual level of grind size differences used in the design below for Coarse, Medium and Fine.

## Design

1. Get three biological samples (3 carrots from different locations, 3 spinach samples from different stores/farms, etc.) from each produce type.  Samples A, B, C
2. For each of these 3 samples use the normal RFC methods for intake but generate 3 separate cups to place dried sample into (rather than 1).  Subsamples A1, A2, A3, B1, B2...
3. Dry the samples.
4. Grind the subsamples so they are visually to the Coarse level.
5. For each subsample, using normal RFC protocols on sampling and packing as part of the XRF procedure, measure on the XRF.  Return the sample just placed on the XRF to the sample bag, mix slightly, resample and repeat.  Repeat this 3 times.  These replicate samples are A1.1, A1.2, A1.3, A2.1, A2.2, A2.3...
6. On the final replicate for each subsample (A1.3, A2.3, A3.3, B1.3, B2.3... etc.) place a flat glass beaker on the top to compress the sample (not so heavy it will break, but heavy enough to compress the sample more than normal).  Call this sample XX.Xb (so A1.3b, A2.3b, etc.)
7. Place the PDZ file for each crop in a separate folder so they can be easily overlayed to check for consistency.
8. Run the calibrations and save the results in a tab in the RFC 2019 XRF spreadsheet on google docs.

In total, this procedure generates the following number of XRF measurements:

**6 crops x 3 samples x 3 subsamples x 4 replicates (3 normal + 1 'packed' replicate) = 216 samples**

example IDs: 
- carrot1.1, carrot1.2, carrot1.3, carrot1.3b
- carrot2.1, carrot2.2, carrot2.3, carrot2.3b
... ...

## Analysis

Once complete, we need to review the data to identify loss in precision, on a by-crop and by-element basis, caused by each of the aforementioned sources.

1. Variation from placement and packing on the XRF
2. Variation from subsampling a single plant (same plant but separated chopped bits into separate dryer cups)
3. Variation from differential grind sizes

Ultimately, by providing an accurate estimate of precision, we can more accurately communicate how much of the variation we are seeing in our final data is due due to a lack of precision and which is due to actual sample variation.
